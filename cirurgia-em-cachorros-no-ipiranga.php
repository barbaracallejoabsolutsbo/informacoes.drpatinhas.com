<?php
    $title       = "Cirurgia em Cachorros no Ipiranga";
    $description = "Com a cirurgia em cachorros no Ipiranga da Dr. Patinhas é possível salvar, prolongar e melhorar a qualidade de vida de cães de diferentes idades e raças. Venha conferir.";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    <?php // include "includes/modal-orcamento-qsmi.php"; ?>

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php // echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>Na clínica Dr. Patinhas, a cirurgia em cachorros no Ipiranga se destaca, pois, contamos com profissionais excepcionais, bem como oferecemos um atendimento personalizado ao cliente, de forma rápida e eficiente, solucionando a sua necessidade com toda presteza e atenção que o seu pet precisa.</p>

<p>Há mais de 6 anos atuando no ramo de cirurgia em cachorros no Ipiranga, estamos sempre atentos às atualizações e tecnologias da área para fornecer o que há de melhor aos clientes. Além disso, a qualquer hora do dia, nós estamos disponíveis para tirar todas as suas dúvidas, com toda presteza e atenção.</p>

<h2>Mais informações sobre cirurgia em cachorros no Ipiranga:</h2>

<p>Quando pensamos em cirurgia em cachorros no Ipiranga, logo imaginamos um problema de saúde grave que poderá afetar o bem-estar do nosso pet. </p>

<p>Em geral, a cirurgia em cachorros no Ipiranga mais comum é a castração, tendo em vista que este é um processo recomendado por todos os especialistas. Ademais, a castração é um procedimento simples e que não apresenta riscos. Entretanto, é uma forma de lembrar que a cirurgia em cachorros no Ipiranga não é um problema, pelo contrário.</p>

<p>Lembrando que, cada cirurgia em cachorros no Ipiranga terá sua particularidade e grau de complexidade. No entanto, você sempre pode ajudar seu pet a passar pelo processo de forma mais tranquila se estiver inteirado no assunto. </p>

<p>É importante frisar que o processo de pré cirurgia em cachorros no Ipiranga  é fundamental, pois, é uma maneira de reforçar a saúde canina, para que o pet esteja mais forte quando for para a mesa de operação e, assim, minimizar possíveis riscos operatórios em animais no geral.   </p>

<h2>Conheça melhor a Dr. Patinhas:</h2>

<p>Em primeiro momento, nós atendemos também animais exóticos, exceto aves. Todos os nossos serviços envolvem qualidade, incluindo vacinas importadas, medicamentos, castração de animais, limpeza de tártaros, piometra e cesárea, além do excelente e cuidadoso tratamento no banho e tosa.</p>

<p>Diante de todos esses fatores, vale salientar que, pensando em você, nós visamos agregar valores acessíveis e justos em conjunto com diversas formas de pagamento para facilitar a sua parceria. E ainda, nós estabelecemos, desde o primeiro contato, uma relação de transparência e comprometimento para que ambas as partes se sintam confortáveis e seguras.</p>
<p>Por fim, frisamos que para melhor atender as necessidades de nossos pacientes, contamos com excelentes colaboradores que ao serem requisitados, prontamente se deslocam à nossa clínica para efetuarem os exames necessários. Não perca mais tempo, ligue agora mesmo e saiba mais.</p>

                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>