<?php
    $title       = "Emergência para animais na Vila Mariana";
    $description = "Contar com salas de emergência e centros cirúrgicos completamente preparados também é fundamental para que a emergência para animais na Vila Mariana possa ter resultados positivos.
";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    <?php // include "includes/modal-orcamento-qsmi.php"; ?>

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php // echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>Emergência para animais na Vila Mariana é com a Dr. Patinhas. Com anos de experiência no ramo, nós possuímos uma completa infraestrutura que permite atendimento rápido em casos de urgência, além da equipe de médicos veterinários amplamente treinada para este tipo de situação.</p>

<p>Em geral, a nossa emergência para animais na Vila Mariana se destaca, pois, oferecemos um atendimento personalizado a cada cliente, bem como estamos sempre atentos as atualizações para fornecer serviços de última geração aos nossos queridos pets.</p>

<h2>Por que a emergência para animais na Vila Mariana é importante?</h2>

<p>Em geral, a emergência para animais na Vila Mariana  consiste no tipo de situação que exige medidas rápidas e precisas para salvar a vida dos animais. Ou seja, é necessário profissionais bem treinados e capacitados para diagnosticar a gravidade dos problemas apresentados e realizar os procedimentos necessários para manter a saúde e a vida dos pets prejudicados.</p>
<p>Na emergência para animais na Vila Mariana é essencial que os profissionais que atuam no atendimento emergencial sejam totalmente capacitados já que, neste momento, qualquer erro pode ser muito significativo para a recuperação ou piora do animal atendido. </p>
<p>De forma sucinta, fica clara a importância de que a emergência para animais na Vila Mariana conte com profissionais e estrutura preparados para e o atendimento emergencial, permitindo que este seja feito de maneira correta e que possa, de fato, salvar a vida do pet doente. </p>
<p>É importante que, equipamentos veterinários, instrumentos para cirurgias e suturas, seringas com drogas de pronto-atendimento e ferramentas de higiene e segurança estejam sempre preparados na emergência para animais na Vila Mariana, possibilitando que os atendimentos de urgência sejam feitos de maneira imediata e segura.</p>
<h2>A melhor emergência para animais na Vila Mariana é na Dr. Patinhas!</h2>
<p>Com uma estrutura sólida e confiável, nós agregamos ainda valores acessíveis e justos em conjunto com diversas formas de pagamento para facilitar a sua parceria. Além disso, a nossa clínica é confortável, climatizada e totalmente higienizada.</p>
<p>A Dr. Patinhas sempre se dedica aos animais como um ser vivo e nunca como um produto. Assim, entendemos que é possível prestar um atendimento de ótima qualidade sem a necessidade de se cobrar valores muitas vezes inacessíveis à muitos clientes. Deixe os detalhes com a nossa equipe e supere as suas expectativas. A qualquer hora do dia, estamos disponíveis para realizar um orçamento sem compromisso, tirar todas as suas dúvidas e fornecer um suporte completo, com toda presteza e atenção que o cliente procura e merece.</p>

                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>