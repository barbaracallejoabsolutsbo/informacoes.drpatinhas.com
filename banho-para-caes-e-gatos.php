<?php
    $title       = "Banho para cães e Gatos";
    $description = "A nossa clínica conta com uma equipe treinada e preparada para cuidar do banho para cães e gatos com todo amor, carinho e delicadeza necessária.
";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    <?php // include "includes/modal-orcamento-qsmi.php"; ?>

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php // echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>A higiene é fundamental para a manutenção da saúde e bem-estar dos nossos amigos de estimação. Portanto, criar o hábito de banho para cães e gatos é extremamente importante para que os pets fiquem protegidos. </p>

<p>É importante frisar que, existem procedimentos corretos para que o banho seja bem realizado e o pet fique realmente limpo e higienizado. Por isso, a Dr. Patinhas se destaca no ramo de banho para cães e gatos, oferecendo soluções completas aos clientes, bem como um atendimento personalizado a cada bichinho de estimação.</p>

<h2>Conhecendo a importância do banho para cães e gatos:</h2>

<p>Conforme supracitado é de suma importância manter o banho para cães e gatos regularmente, tendo em vista que a sujeira acumulada na pele e na pelagem de cães e gatos, pode causar mal cheiro e uma série de doenças dermatológicas.</p>

<p>É importante salientar que, cada cão ou gato, cada espécie, porte, entre outros possuem necessidades específicas com relação à frequência de banho para cães e gatos. Por isso, é importante ficar atento à necessidade de seu pet em específico. Vejamos abaixo:</p>
<ul>
<li>
<p>Banho em gatos: As lambidas não são capazes de eliminar a sujeira acumulada profundamente na pele e as bactérias presentes nela. Por isso, se faz necessário que os gatos sejam acostumados desde a infância a tomarem banhos e serem escovados com certa frequência.</p>
</li>
<li>
<p>Banho em cães: O ambiente no qual o cão vive, seus hábitos, raça, porte e se possuem muitos ou poucos pêlos influenciam diretamente na frequência dos banhos e da tosa. Lembrando que, cães com a pelagem mais espessa e longa precisam ser escovados com maior frequência e tosados periodicamente.</p>
</li>
</ul>
<p>Vale lembrar que, o banho para cães e gatos requer cuidados especiais de profissionais habilitados, pois, cão ou gato com problemas dermatológicos precisam de produtos especiais, medicamentos e frequência diferenciada.</p>
<h2>Por que realizar o banho para cães e gatos com Dr. Patinhas?</h2>
<p>No momento em que entrar em contato com a nossa equipe, você notará que encontrou a clínica ideal para o banho para cães e gatos. Além disso, agregamos o melhor custo benefício do mercado em conjunto com diversas formas de pagamento para facilitar a sua parceria.</p>
<p>Dentre os diversos benefícios que oferecemos, a cirurgia é a especialidade médica que realiza procedimentos invasivos com finalidade terapêutica, e/ou diagnóstica. Por isso, realizamos cirurgias eletivas, emergenciais e de alta complexidade com maior qualidade e eficiência no ramo. Se interessou? Não perca mais tempo, entre em contato conosco a qualquer hora do dia, tire todas as suas dúvidas e faça um orçamento sem compromisso. Esperamos por você.</p>

                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>