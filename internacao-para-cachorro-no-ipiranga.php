<?php
    $title       = "Internação para Cachorro no Ipiranga";
    $description = "A internação para cachorro no Ipiranga nada mais é do que um procedimento de cuidado diário que acontece na unidade de saúde para tratamento aos pets necessitados.
";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    <?php // include "includes/modal-orcamento-qsmi.php"; ?>

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php // echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>A internação para cachorro no Ipiranga acontece após o atendimento com o profissional qualificado depois de um diagnóstico específico. Se necessário, o mesmo solicita exames de imagem e sangue para diagnósticos mais precisos para iniciar o tratamento adequado ao pet.</p>

<p>Sabendo dessa importância, a Dr. Patinhas não hesita em fazer um bom trabalho e oferece as melhores soluções para a internação para cachorro no Ipiranga, através de um atendimento totalmente personalizado e soluções modernas, tendo em vista que estamos sempre atentos às atualizações do ramo.</p>

<h2>Principais motivos que causam a internação para cachorro no Ipiranga:</h2>
<h2></h2>
<p>A internação para cachorro no Ipiranga é considerada como um procedimento de cuidado diário que acontece na unidade de saúde, sendo indicado para tratamentos que precisam que o paciente fique mais de 24 horas no hospital. </p>
<p>As clínicas, geralmente, contam com os equipamentos, estrutura e profissionais especializados para diagnosticar e tratar os pets. Portanto, quando você perceber que seu pet está com algum comportamento diferente ou desanimado, uma visita ao veterinário é sempre importante, pois, os pets podem ficar no pronto atendimento, na unidade de internação para cachorro no Ipiranga e na unidade de terapia intensiva. </p>
<p>Pois bem, existem diversas situações que levam os cães a internação para cachorro no Ipiranga em unidades de atendimento para pets. Entre esses motivos, podemos identificar os que sofrem acidente e os que são acometidos por alguma doença. Entre outros, podemos citar alguns problemas de saúde, como:</p>
<ul>
<li>
<p>Pancreatite;</p>
</li>
<li>
<p>Doenças do carrapato;</p>
</li>
<li>
<p>Coronavirose canina;</p>
</li>
<li>
<p>Parvovirose canina, entre outros.</p>
</li>
</ul>
<p>Para alguns donos, a necessidade do acompanhamento médico pode significar um caso muito grave. No entanto, é preciso entender que a internação para cachorro no Ipiranga é indicada quando a intervenção não pode ser feita em casa. </p>
<h2>Faça a internação para cachorro no Ipiranga com Dr. Patinhas!</h2>
<p>Após 6 anos de acompanhamento diário em cirurgias, enfermidades, distúrbios e disfunções apresentadas pelas diversas raças e espécies de animais, conquistamos o nosso espaço no ramo e buscamos superar as expectativas de nossos clientes em todos os aspectos.</p>
<p>Diante de todos esses fatores, vale salientar que, nós visamos agregar valores acessíveis e justos em conjunto com diversas formas de pagamento para facilitar a sua parceria. E ainda, desde o início, é estabelecida uma relação de transparência e comprometimento para que ambas as partes se sintam seguras e confiáveis na relação. Ligue agora mesmo e saiba mais.</p>

                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>