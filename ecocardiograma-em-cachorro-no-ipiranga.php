<?php
    $title       = "Ecocardiograma em Cachorro no Ipiranga";
    $description = "De forma resumida, o ecocardiograma em cachorro no Ipiranga nada mais é do que um tipo de exame obtido por meio da ultrassonografia cardíaca. Venha saber mais.
";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    <?php // include "includes/modal-orcamento-qsmi.php"; ?>

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php // echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>O ecocardiograma em cachorro no Ipiranga destaque no ramo é na Dr. Patinhas, uma clínica especializada que também atende animais exóticos. Com anos de experiência, não hesitamos em fazer um bom trabalho e contamos com uma equipe unida e organizada para superar todas as suas expectativas, com serviços de qualidade.</p>
<p>Em suma, um ecocardiograma em cachorro no Ipiranga é um exame por imagem que permite o diagnóstico de diversas doenças em cães e também, em gatos. As patologias cardíacas em animais de pequeno porte podem ser graves e fulminantes, e o diagnóstico precoce é sempre a melhor opção para tratamento adequado.</p>
<h2>Importância do ecocardiograma em cachorro no Ipiranga:</h2>
<p>O ecocardiograma em cachorro no Ipiranga  é um tratamento que permite ondas sonoras ao coração do bichinho, captando os ecos formados e os transforma em imagens que são registradas e visualizadas em um monitor que possibilita a leitura e o diagnóstico por parte do médico veterinário.</p>
<p>Lembrando que, o ecocardiograma em cachorro no Ipiranga não é um procedimento invasivo, não sendo necessário realizar nenhum tipo de corte e nem mesmo colocar o pet sob sedação </p>
<p>Além disso, é importante salientar que, o ecocardiograma em cachorro no Ipiranga também oferece vantagens com relação a um exame de raios x, como o fornecimento de imagens mais detalhadas e a ausência de emissão de radiação.</p>
<p>A aquisição de aparelhos tecnológicos para o ecocardiograma em cachorro no Ipiranga oferece uma vantagem competitiva no mercado com relação aos concorrentes e ainda contribui para a fidelização dos clientes. Consulte um profissional adequado.</p>
<h2>Por que realizar o ecocardiograma em cachorro no Ipiranga com a Dr. Patinhas?</h2>
<p>Os benefícios são diversos, porém, entre eles nós agregamos valores acessíveis e justos em conjunto com diversas formas de pagamento para facilitar a sua parceria. E ainda, desde o primeiro contato, nós estabelecemos uma relação de transparência e comprometimento para que ambas as partes se sintam confortáveis e seguras nesta parceria.</p>
<p>Por fim, o nosso consultório é confortável, climatizado e higienizado, para o seu pet, com a ideia é deixar o bichinho o mais calmo possível, para o contato com o veterinário na hora da consulta ser tranqüilo. </p>
<p>No momento em que entrar em contato com a nossa equipe, você notará que encontrou a clínica ideal para se tornar o mais novo parceiro de longa data. Não perca essa oportunidade, ligue agora mesmo e faça um orçamento sem compromisso. Nós esperamos por você.</p>

                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>