<?php
$title       = "Oftalmologia para animais em Bonsucesso - Guarulhos";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>O veterinário especializado em Oftalmologia para animais em Bonsucesso - Guarulhos atende principalmente pequenos animais, como cães e gatos, fazendo o diagnóstico e o tratamento de todas as doenças que atingem os olhos. Se faz necessário que as visitas sejam constantes ao oftalmologia para a realização de check-ups oculares e também são essenciais nos casos em que os animais apresentam sintomas como alterações.</p>
<p>A empresa Dr Patinhas é destaque entre as principais empresas do ramo de Clinica Veterinária, vem trabalhando com o princípio de oferecer aos seus clientes e parceiros o melhor em Oftalmologia para animais em Bonsucesso - Guarulhos do mercado. Ainda, possui facilidade com Veterinário Dermatológico, Vacina contra Raiva, Oftalmologia para animais, Ortopedia Animal Veterinário e Raio X em gatos mantendo a mesma excelência. Pois, contamos com a melhor equipe da área em que atuamos a diversos anos.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>