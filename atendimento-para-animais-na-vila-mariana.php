<?php
    $title       = "Atendimento para animais na Vila Mariana";
    $description = "Os bichos de estimação recebem toda a atenção e medicação completa desde o primeiro atendimento para animais na Vila Mariana na clínica Dr. Patinhas.
";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    <?php // include "includes/modal-orcamento-qsmi.php"; ?>

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php // echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>O atendimento para animais na Vila Mariana oferecido pela equipe Dr. Patinhas se destaca no ramo por oferecer soluções completas, de forma rápida e eficiente. Além disso, trabalhamos de forma unida e organizada para não somente atender a necessidade do cliente, mas também, superar as suas expectativas.</p>

<p>Conosco, o atendimento para animais na Vila Mariana é preparado para que o pet fique tranqüilo e a consulta flua com mais leveza. Além disso, o nosso consultório é confortável, climatizado e higienizado.</p>
<h2>Saiba como funciona o atendimento para animais na Vila Mariana em casos de internação:</h2>
<p>Em primeiro lugar, é importante saber que a internação pode ser uma etapa importante para garantir o melhor tratamento e atendimento para animais na Vila Mariana de qualidade.</p>
<p>De forma geral, é necessário que as clínicas contam com uma unidade de atendimento para animais na Vila Mariana bem preparada para receber os animais que chegam em situações de urgência e emergência, com equipamentos próprios, como por exemplo: as bombas de infusão, de oxigênio e ar comprimido devem ser disponibilizadas logo no pronto atendimento para animais na Vila Mariana.</p>
<p>No início do atendimento para animais na Vila Mariana, os animais devem receber a análise do médico veterinário, que observa os sinais vitais do animal, sintomas e realiza o controle da dor. Caso seja necessário, o profissional indica a internação do animal e solicita exames de imagem e sangue para diagnósticos mais precisos.</p>
<p>Ao apresentar evolução no quadro clínico, o animal de estimação é avaliado pelo veterinário responsável para analisar a liberação do bicho de estimação, no qual exames finais são realizados para uma conclusão mais precisa.</p>
<h2>Bem vindo ao melhor atendimento para animais na Vila Mariana!</h2>
<p>Conosco, você tem a tranquilidade de receber serviços de qualidade ao seu pet, através de uma equipe especializada. Pensando nisso, nós ainda agregamos valores acessíveis e justos em conjunto com diversas formas de pagamento para facilitar a sua parceria.</p>
<p>É importante frisar que, também atendemos animais exóticos, exceto aves. No entanto, contamos com excelentes colaboradores para efetuar os exames necessários, bem como oferecemos uma infraestrutura de atendimento rápido e médicos veterinários completamente treinados para casos de emergência. No momento em que entrar em contato conosco, você notará que fez a escolha certa. Deixe os detalhes conosco e desfrute de um trabalho bem feito. Ligue agora mesmo, tire todas as suas dúvidas com os nossos profissionais, realize um orçamento sem compromisso e tenha a certeza de que fez a escolha certa. Esperamos por você.</p>

                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>