<?php
$title       = "Hospital veterinário em São Mateus";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Em geral, um Hospital veterinário em São Mateus em é um ambiente com mais estrutura e setores variados, atendendo mais animais, devido ao volume maior de profissionais disponíveis. O ambiente ambulatório do hospital é um dos setores mais importantes, tendo em vista que está apto a receber animais de médio e grande porte para uma avaliação inicial e uma triagem. Venha conhecer nossa estrutura!</p>
<p>Como uma empresa especializada em Clinica Veterinária proporcionamos sempre o melhor quando falamos de Cirurgia em Animais, Clínica veterinária de Domingo, Exames laboratoriais para Cachorro, Exames para Gatos e Ecocardiograma em Cachorro. Com potencial necessário para garantir qualidade e excelência em Hospital veterinário em São Mateus com custo x benefício justos no mercado sem diminuir a qualidade de nossa especialidade. Nós da empresa Dr Patinhas trabalhamos com os melhores valores do mercado em que atuamos.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>