<?php
    $title       = "Produtos Hydra para seu animal";
    $description = "Os produtos hydra para seu animal têm como fundamento básico respeitar os clientes e os seus pets no ambiente onde estão inseridos.Venha saber mais.
";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    <?php // include "includes/modal-orcamento-qsmi.php"; ?>

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php // echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>Está procurando por produtos hydra para seu animal? Na Dr. Patinhas, você encontra os melhores, além de ter um atendimento totalmente personalizado. Há mais de 6 anos no ramo, não hesitamos em fazer um bom trabalho e buscamos superar as suas expectativas oferecendo soluções completas.</p>

<p>Os produtos hydra para seu animal são desenvolvidos exclusivamente para os profissionais de estética animal. Criados com alta tecnologia e sob consultoria dos melhores profissionais do mercado proporcionam o resultado impecável que você procura. Ao seu pet.</p>

<h2>Vantagens dos produtos hydra para seu animal:</h2>

<p>Uma das mais reconhecidas marcas de mercado pet, a Pet Society oferece produtos hydra para seu animal aliando o amplo conhecimento científico nas áreas de ingredientes e formulações seguras às necessidades do mercado veterinário. </p>
<p>Em geral, ela cria produtos hydra para seu animal exclusivos de alta qualidade e traz novos conceitos que beneficiem e ajudem a modernizar o mercado pet. </p>
<p>Oferece produtos hydra para seu animal seguros para contribuir com o desenvolvimento do mercado pet, criando valor para todos os envolvidos nas nossas operações. Além disso, aumenta a interação e respeito entre os pets e seus tutores. </p>
<p>Por fim, os produtos hydra para seu animal se destacam no ramo, pois, promovem o desenvolvimento sustentável e aumentam o impacto positivo na sociedade com objetivos compartilhados entre fundadores e por todos os colaboradores.</p>
<h2>A Dr. Patinhas utiliza os melhores produtos hydra para seu animal </h2>
<p>A nossa clínica conta com uma equipe treinada e preparada para cuidar da estética do seu pet com todo amor. E ainda, pensando em você, agregamos valores acessíveis e justos em conjunto com diversas formas de pagamento para facilitar a sua aquisição.</p>
<p>Para melhor atender as necessidades de nossos pacientes, contamos com excelentes colaboradores que ao serem requisitados, prontamente se deslocam à nossa clínica para efetuarem os exames necessários. E ainda, dentre os nossos diversos serviços, oferecemos vacinas importadas, medicamentos, castração de animais, limpeza de tártaro, piometra e cesárea, além do excelente e cuidadoso tratamento no banho e tosa.</p>
<p>Ressaltamos ainda que, desde o primeiro contato, estabelecemos uma relação de transparência e comprometimento para que ambas as partes se sintam confortáveis e seguras nos procedimentos executados nos pets. No momento em que entrar em contato conosco, você notará os nossos diferenciais e terá a certeza de que encontrou a clínica ideal para cuidar de seu pet e se tornar o mais novo parceiro de longa data. Ligue agora mesmo e saiba mais.</p>

                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>