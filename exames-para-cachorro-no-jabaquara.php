<?php
    $title       = "Exames para Cachorro no Jabaquara";
    $description = "Com os exames para cachorro no Jabaquara, você garantirá a saúde no médio e longo prazo do seu animal de estimação, pois muitas coisas poderão ser previstas e, assim, tratadas.
";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    <?php // include "includes/modal-orcamento-qsmi.php"; ?>

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php // echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>Existem muitos tipos de exames para cachorro no Jabaquara </p>
<p>de rotina. Alguns mais simples, outros um pouco mais complexos. No entanto, todos são igualmente importantes para manter o nosso melhor amigo sempre saudável. Neste sentido, a Dr. Patinhas se destaca por apresentar soluções completas, bem como um atendimento personalizado a cada cliente.</p>
<p>É importante salientar que, o veterinário solicitar exames para cachorro no Jabaquara é normal, pois, com os exames necessários, o profissional poderá avaliar a saúde do seu peludo por completo, de forma mais segura e eficiente.</p>
<h2>Conheça os principais exames para cachorro no Jabaquara:</h2>
<p>Para poder fazer uma avaliação completa da saúde do coração do seu pet, o médico veterinário poderá solicitar diversos exames para cachorro no Jabaquara, entre eles: a realização de um ecocardiograma em cachorro que ajuda a avaliar o coração do bichinho.</p>
<p>Por sua vez, entre os exames para cachorro no Jabaquara também se destaca a endoscopia que serve tanto para diagnóstico como para tratamento, tendo em vista que é um tubo super flexível que permite avaliar alguns órgãos internos, sem que o pet não precise passar por uma cirurgia.</p>
<p>Os exames para cachorro no Jabaquara de sangue, conhecidos também como, hemograma e o bioquímico. No de sangue, o profissional pode solicitar por exemplo a contagem de plaquetas, de células vermelhas e brancas, teste de compatibilidade sanguínea, contagem de reticulócitos, entre outros.</p>
<p>Já os exames para cachorro no Jabaquara de bioquímica são solicitados para avaliar se os rins, o pâncreas e outros órgãos dos cães estão funcionando bem. </p>
<p>Diante de todos esses fatores, é necessário consultar um profissional adequado para fornecer as informações adequadas, bem como solicitar os exames ideais.</p>
<h2>Os melhores exames para cachorro no Jabaquara!</h2>
<p>Conosco, você encontra o melhor custo benefício do mercado em conjunto com diversas formas de pagamento para facilitar a sua parceria. E ainda, o nosso consultório é confortável, climatizado e higienizado, para o seu pet, pois, a nossa idéia é deixar o bichinho o mais calmo possível, para o contato com o veterinário na hora da consulta ser tranqüilo.</p>
<p>Desde o primeiro contato, é estabelecida uma relação de transparência e comprometimento para que ambas as partes se sintam confortáveis e seguras na relação. Além disso, a nossa equipe está sempre atenta às atualizações do ramo para fornecer o que há de melhor e mais moderno aos nossos pets, desde a estética até os procedimentos cirúrgicos. Entre em contato conosco e faça um orçamento sem compromisso.</p>

                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>