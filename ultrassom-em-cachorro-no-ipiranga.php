<?php
$title       = "Ultrassom em cachorro no Ipiranga";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>O Ultrassom em cachorro no Ipiranga é um dos exames de imagem mais comuns nos centros veterinários, sendo usado para visualização precisa dos órgãos internos dos cachorros. Na Dr. Patinhas, este exame serve para auxiliar no diagnóstico. Com anos de experiência no ramo de ultrassom, contamos com profissionais qualificados que estão sempre atualizados fornecendo o que há de melhor e mais moderno aos clientes.</p>
<p>Na busca por uma empresa referência, quando o assunto é Clinica Veterinária, a Dr Patinhas será sempre a escolha que mais se destaca entre as principais concorrentes. Pois, além de fornecedor de Internação para Gatos, Cirurgia em Cachorros, Clínica de Estética animal, Raio X em cachorro e Vacinas para animais, oferece Ultrassom em cachorro no Ipiranga com a melhor qualidade da região, também visa garantir o melhor custo x benefício, com agilidade e dedicação para você.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>