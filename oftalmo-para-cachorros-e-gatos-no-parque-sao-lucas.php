<?php
$title       = "Oftalmo para cachorros e Gatos no Parque São Lucas";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Oftalmo para cachorros e Gatos no Parque São Lucas, em geral, serve para tratar as doenças oculares em cães, gatos e outros animais, realizando os exames e dando os diagnósticos necessários. Sabendo dessa importância, a Dr. Patinhas é uma clínica séria que não hesita em fazer um bom trabalho e conta com uma equipe qualificada para atender a necessidade do cliente, de forma rápida e eficiente.</p>
<p>Com a Dr Patinhas proporcionando o que se tem de melhor e mais moderno no segmento de Clinica Veterinária consegue garantir aos seus clientes a confiança e conforto que todos procuram. Com o melhor em Ortopedia Animal Veterinário, Hospital veterinário, Clínica veterinária de Domingo, Clínica de Pet Shop e Hospital para cães e gatos nossa empresa, hoje, consegue possibilitar diversas escolhas para os melhores resultados, ganhando destaque e se tornando referência em Oftalmo para cachorros e Gatos no Parque São Lucas.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>