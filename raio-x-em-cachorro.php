<?php
    $title       = "Raio X em cachorro";
    $description = "O raio X em cachorro da Dr. Patinhas pode ser usado com anestesia  em casos de animais agressivos que podem colocar em risco o médico veterinário.
";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    <?php // include "includes/modal-orcamento-qsmi.php"; ?>

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php // echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>O raio X em cachorro pode diagnosticar antecipadamente alguma patologia e dimensionar danos causados por lesões, permitindo a realização de um tratamento apropriado. Por isso, a Dr. Patinhas não hesita em fazer um bom trabalho e busca superar as expectativas, oferecendo um atendimento totalmente personalizado ao cliente.</p>

<p>Em geral, o raio X em cachorro é uma das técnicas mais importantes, pois, é amplamente utilizado na realização de diagnósticos mais eficazes para diversos órgãos e regiões do corpo dos bichinhos. Além disso, a qualquer hora do dia, nós estamos disponíveis para tirar todas as suas dúvidas, com presteza e atenção.</p>

<h2>Entenda melhor o raio X em cachorro:</h2>

<p>O raio X em cachorro possibilita aos veterinários visualizar em detalhes os tecidos ósseos e órgãos internos dos animais.</p>
<p>Existem dois tipos de raio X em cachorro comuns. Vamos conhecê-los:</p>
<ul>
<li>
<p>Raio-x digital - É o meio mais prático e fácil, pois não irrita o pet. Esse procedimento dura no máximo 20 minutos;</p>
</li>
<li>
<p>Raio-x contrastado – É o procedimento mais complexo para os animais, por conta do contato com substâncias químicas que precisam ser injetadas nos pets. </p>
</li>
</ul>

<p>Os casos mais comuns em que os médicos veterinários pedem exames de raio X em cachorro é para examinar tecidos ósseos e articulações nos casos de ingestão de objetos estranhos  e também em caso de tumores. </p>
<p>Por isso, caso precise de raio X em cachorro conte com uma clínica especializada que possui equipamentos modernos de última geração que permite radiografar cachorros de grande porte com imagens de alta qualidade. </p>
<h2>Por que fazer o raio X em cachorro com a Dr. Patinhas?</h2>
<p>Primeiramente, nós possuímos uma completa infraestrutura que permite atendimento rápido em casos de urgência, além da equipe de médicos veterinários amplamente treinada para este tipo de situação. E ainda, oferecemos valores acessíveis em conjunto com diversas formas de pagamento para facilitar a sua contratação.</p>
<p>Após 6 anos de acompanhamento diário em cirurgias, enfermidades, distúrbios e disfunções apresentadas pelas diversas raças e espécies de animais, a nossa experiente equipe obteve a grande oportunidade de compartilhar os seus conhecimentos de maneira totalmente independente, formando seu próprio centro de atendimento veterinário. Por isso, caso haja dúvidas, a qualquer hora do dia, nós estamos disponíveis para tirar todas as suas dúvidas. Deixe os detalhes com a nossa equipe e faça um orçamento sem compromisso conosco. Ligue agora mesmo e tire todas as suas dúvidas. Esperamos por você em qualquer horário.</p>

                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>