<?php
    $title       = "Ortopedia Animal Veterinário";
    $description = "Para ortopedia animal veterinário, a clínica Dr. Patinhas apresenta diversos tratamentos, inclusive em animais exóticos, exceto aves. Venha saber mais.
";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    <?php // include "includes/modal-orcamento-qsmi.php"; ?>

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php // echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>A ortopedia animal veterinário nada mais é que uma especialidade que estuda, diagnostica e trata casos de traumatologias e patologias relacionadas aos ossos, músculos, articulações e ligamentos dos pets. Por isso, a Dr. Patinhas não hesita em fazer um bom trabalho e conta com uma equipe especializada para fornecer soluções completas.</p>

<p>Atualmente, somos referência em ortopedia animal veterinário, pois, estamos sempre atentos as atualizações do ramo para fornecer serviços de última geração. Além disso, a qualquer hora do dia, nós estamos disponíveis para tirar todas as suas dúvidas e fornecer todo o suporte necessário, com presteza e atenção.</p>

<h2>Detalhes da ortopedia animal veterinário:</h2>

<p>A ortopedia animal veterinário em pequenos animais cuida da estrutura esquelética dos mesmos, sendo considerada uma das especialidades que mais crescem. A locomoção faz parte da qualidade de vida dos pets.</p>
<p>O que muitos tutores esperam e que estes animais consigam alcançar a velhice mantendo o bem-estar. Assim, existe uma demanda muito grande por serviços de ortopedia animal veterinário.</p>
<p>Em geral, a ortopedia animal veterinário é mais indicada para fazer cirurgia em casos de atropelamento, quedas, entre outros, e também proporcionar a reabilitação ortopédica do animal após fraturas e luxações.</p>
<p>Conheça alguns tratamentos de ortopedia animal veterinário:</p>
<ul>
<li>
<p>Fisioterapia: Relaxa e fortalece a musculatura para aliviar a dor;</p>
</li>
<li>
<p>Hidroterapia: Possui o objetivo relaxar os músculos e fortalecê-los para reabilitar rapidamente o animal;</p>
</li>
<li>
<p>Acupuntura: É método terapêutico chinês que tem como propósito proporcionar equilíbrio energético ao corpo, entre outros.</p>
</li>
</ul>
<p>Consulte um profissional adequado para tratar de seu pet, pesquise a fundo antes de tomar quaisquer decisões precipitadas.</p>
<h2>Vantagens da ortopedia animal veterinário da Dr. Patinhas:</h2>
<p>Além do melhor custo benefício do mercado, nós oferecemos diversas formas de pagamento para facilitar a sua parceria. E ainda, desde o primeiro contato, é estabelecida uma relação de transparência e comprometimento para que ambas as partes se sintam seguras e confortáveis.</p>
<p>Para melhor atender as necessidades de nossos pacientes, contamos com excelentes colaboradores que ao serem requisitados, prontamente se deslocam à nossa clínica para efetuarem os exames necessários. No momento em que entrar em contato conosco, você notará os nossos diferenciais e terá a certeza de que encontrou a clínica ideal para cuidar de seu pet e se tornar o mais novo parceiro de longa data.Deixe os detalhes com a nossa equipe e desfrute de um trabalho bem feito. Entre em contato agora mesmo e realize um orçamento sem compromisso.</p>

                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>