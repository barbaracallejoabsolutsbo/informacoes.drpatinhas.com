<?php
    $title       = "Clínica veterinária de Domingo";
    $description = "A nossa clínica veterinária de domingo oferece diversos serviços, atendendo também, animais exóticos com exceção das aves. Venha saber mais.
";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    <?php // include "includes/modal-orcamento-qsmi.php"; ?>

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php // echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>A Dr. Patinhas é uma clínica veterinária de domingo que oferece um atendimento personalizado ao cliente e ao pet, através de profissionais qualificados. Além disso, a qualquer hora do dia, nós estamos disponíveis para tirar todas as suas dúvidas, com toda presteza e atenção necessária.</p>

<p>De forma conclusiva, somos uma clínica veterinária de domingo que oferece diversos serviços de qualidade. Entre eles: vacinas importadas, medicamentos, castração de animais, limpeza de tártaro, piometra e cesárea, além do excelente e cuidadoso tratamento no banho e tosa.</p>

<h2>Como obter uma clínica veterinária de domingo?</h2>

<p>Em geral, para montar uma clínica veterinária de domingo são necessários dois fatores essenciais. Como por exemplo:</p>

<ul>
<li>
<p>Noções básicas de administração de negócios;</p>
</li>
<li>
<p>Um plano de negócios profissional e bem montado.</p>
</li>
</ul>
<p>Pois bem, uma clínica veterinária de domingo  é definida como uma empresa que faz o atendimento de animais para consultas e tratamento clínico-cirúrgicos, devendo ter a presença de um médico veterinário devidamente registrado, que terá a responsabilidade técnica pelos atendimentos.</p>
<p>Lembrando que, será necessário lidar com a parte burocrática da clínica veterinária de domingo, que é registrar a sua clínica na junta comercial da sua cidade, além do Conselho Regional de Medicina Veterinária e obter a licença da Vigilância Sanitária.</p>
<p>Além disso, para uma clínica veterinária de domingo adequada, deverá apresentar técnicos laboratoriais, atendentes e, caso a clínica funcione 24 horas por dia, uma equipe credenciada para ficar o dia todo.</p>
<h2>Venha conhecer a melhor clínica veterinária de domingo!</h2>
<p>Após 6 anos de acompanhamento diário em cirurgias, enfermidades, distúrbios e disfunções apresentadas pelas diversas raças e espécies de animais, a nossa experiente equipe obteve a grande oportunidade de compartilhar seus conhecimentos de maneira totalmente independente, formando seu próprio centro de atendimento veterinário. </p>
<p>Sabendo de toda importância do trabalho, nós agregamos valores acessíveis e justos em conjunto com diversas formas de pagamento para facilitar a sua parceria.  Além disso, o nosso consultório é confortável, climatizado e higienizado, para o seu pet, com o objetivo de deixar o bichinho o mais calmo possível, para o contato com o veterinário na hora da consulta ser mais tranqüilo.  Não perca mais tempo e nem a oportunidade de se tornar parceiro de uma clínica que prioriza e respeita o seu pet, oferecemos um atendimento personalizado, bem como serviços e atendimento de qualidade e modernidade inigualável aos demais. Ligue agora mesmo e tire todas as suas dúvidas. Nós estamos esperando por você.</p>

                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>