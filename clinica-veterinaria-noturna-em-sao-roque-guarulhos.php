<?php
$title       = "Clínica veterinária noturna em São Roque - Guarulhos";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p> A Dr. Patinhas se destaca como Clínica veterinária noturna em São Roque - Guarulhos, pois, além de segurança completa, nós oferecemos um atendimento personalizado aos clientes e pets, bem como soluções completas e rápidas. Além disso, a qualquer hora do dia, estamos disponíveis para fornecer todo o suporte necessário, com presteza e atenção. Além de que temos melhor custo benefício do mercado.</p>
<p>Com uma alta credibilidade no mercado de Clinica Veterinária, proporcionando com qualidade, viabilidade e custo x benefício em Clínica veterinária noturna em São Roque - Guarulhos, a empresa Dr Patinhas vem crescendo e mostrando seu potencial através, também de Hospital veterinário, Produtos Hydra para seu animal, Ortopedia Animal Veterinário, Banho e tosa para animais e Oftalmo para cachorros e Gatos, garantindo assim seu sucesso no mercado em que atua. Venha você também e faça um orçamento com um de nossos especialistas no ramo.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>