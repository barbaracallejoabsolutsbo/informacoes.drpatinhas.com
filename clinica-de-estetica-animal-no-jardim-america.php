<?php
$title       = "Clínica de Estética animal no Jardim América";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Em geral, uma Clínica de Estética animal no Jardim América é uma opção para quem deseja se diferenciar da concorrência e montar um pet shop de sucesso, onde os clientes poderão levar seus amigos de quatro patas para um dia especial com banho e tosa, atendimento veterinário, espaço para atividades físicas, brincadeiras, entre outros benefícios. Nos consulte a qualquer momento para tirar suas dúvidas.</p>
<p>Na busca por uma empresa referência, quando o assunto é Clinica Veterinária, a Dr Patinhas será sempre a escolha que mais se destaca entre as principais concorrentes. Pois, além de fornecedor de Melhor pet shop para banho e tosa, Clínica para Animais, Castração de Gato, Oncologia em cães e Gatos e Clínica veterinária noturna, oferece Clínica de Estética animal no Jardim América com a melhor qualidade da região, também visa garantir o melhor custo x benefício, com agilidade e dedicação para você.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>