<?php
$title       = "Cirurgia de tártaro em cães em Diadema";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>De forma resumida, a Cirurgia de tártaro em cães em Diadema é a especialidade médica que realiza procedimentos invasivos com finalidade terapêutica, e diagnóstica. Neste sentido, a Dr. Patinhas, com anos de experiência no ramo, realiza também cirurgias eletivas, emergenciais e de alta complexidade. E ainda, oferecemos todo o atendimento personalizado que o cliente e seu pet, procuram e merecem.</p>
<p>Tendo como especialidade Hospital para cães e gatos, Cirurgia de tártaro em cães, Castração de Gato, Ecocardiograma em Cachorro e Hospital para Cachorro e gato, nossos profissionais possuem ampla experiência e conhecimento avançado no segmento de Clinica Veterinária. Por isso, quando falamos de Cirurgia de tártaro em cães em Diadema, buscar pelos membros da empresa Dr Patinhas é a melhor forma para alcançar seus objetivos de forma rápida e garantida. Entre em contato. Nós podemos te ajudar.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>