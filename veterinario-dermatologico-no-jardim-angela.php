<?php
$title       = "Veterinário Dermatológico no Jardim Ângela";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Fornecemos Veterinário Dermatológico no Jardim Ângela, com um atendimento personalizado. Além disso, a nossa equipe trabalha de forma unida e organizada para fornecer o que há de melhor aos clientes, de acordo com as evoluções do mercado.  O veterinário nada mais é que o médico responsável pela saúde de pele dos animais de estimação, no qual nesta área estão incluídos também os cuidados com as unhas e as orelhas.</p>
<p>Desempenhando uma das melhores assessorias do segmento de Clinica Veterinária, a Dr Patinhas se destaca no mercado, uma vez que, conta com os melhores recursos da atualidade, de modo a fornecer Veterinário Dermatológico no Jardim Ângela com eficiência e qualidade. Venha e faça uma cotação com um de nossos atendentes especializados em Laboratório para animais, Produtos Hydra para seu animal, Raio X em cachorro, Vacinas para animais e Cirurgia de tártaro em cães, pois somos uma empresa especializada.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>