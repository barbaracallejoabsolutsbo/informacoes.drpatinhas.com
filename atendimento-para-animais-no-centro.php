<?php
$title       = "Atendimento para animais no Centro";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>No início do Atendimento para animais no Centro, eles devem receber a análise do médico veterinário, que observa os sinais vitais do animal e realiza o controle da dor. Caso seja necessário, o profissional indica a internação do animal e solicita exames de imagem e sangue para diagnósticos mais precisos. Ao apresentar evolução no quadro clínico, o animal é avaliado para analisar a liberação do mesmo</p>
<p>Especialista no segmento de Clinica Veterinária, a Dr Patinhas é uma empresa diferenciada, com foco em atender de forma qualificada todos os clientes que buscam por Atendimento para animais no Centro. Trabalhando com o foco em proporcionar a melhores experiência para seus clientes, nossa empresa conta com um amplo catálogo para você que busca por Produtos Hydra para seu animal, Hospital para Cachorro e gato, Acupuntura para animais, Clínica veterinária noturna e Exames para Cachorro e muito mais.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>