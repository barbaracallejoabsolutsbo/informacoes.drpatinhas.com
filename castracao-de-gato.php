<?php
$title       = "Castração de Gato";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Pensou em castração de gato, pensou na clínica Dr. Patinhas. Com anos de experiência na área, não hesitamos em fazer um bom trabalho e buscamos superar as expectativas de nossos clientes, apresentando soluções completas, rápidas e eficientes. Além disso, a qualquer hora do dia, estamos disponíveis para fornecer todo o suporte necessário, com presteza e atenção.</p><h2> A melhor castração de gato está na Dr. Patinhas</h2><p>A castração de gato reduz acidentes, brigas e transmissão de doenças infecto-contagiosas, como por exemplo o vírus da imunodeficiência felina. Além disso, reduz o odor da urina dos felinos machos. De forma conclusiva, a castração pode ser realizada de forma eletiva em gatos adultos, desde que o animal não apresente condição de saúde restritiva que torne a cirurgia contraindicada.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>