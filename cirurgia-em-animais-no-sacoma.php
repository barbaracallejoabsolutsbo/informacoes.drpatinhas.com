<?php
    $title       = "Cirurgia em Animais no Sacomã";
    $description = "A nossa cirurgia em animais no Sacomã se destaca, pois, nos dedicamos sempre aos animais como um ser vivo e nunca como um produto. Venha saber mais.";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    <?php // include "includes/modal-orcamento-qsmi.php"; ?>

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php // echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>Com a cirurgia em animais no Sacomã da Dr. Patinhas, você tem soluções completas e eficientes, com toda a modernidade e segurança que procura e merece. Além disso, todos os nossos profissionais oferecem um atendimento personalizado ao cliente, com presteza e atenção.</p>

<p>Para melhor atender as necessidades de nossos pacientes que buscam por cirurgia em animais no Sacomã, contamos com excelentes colaboradores que ao serem requisitados, prontamente se deslocam à nossa clínica para efetuarem os exames necessários e demais soluções.</p>
<h2>Cirurgia em animais no Sacomã:</h2>
<p>O primeiro passo para realizar a cirurgia em animais no Sacomã é verificar como está a saúde do pet e quais os procedimentos necessários. Para isso, o veterinário vai realizar algumas consultas e exames. </p>
<p>No dia anterior à cirurgia em animais no Sacomã, o profissional veterinário vai passar uma série de recomendações. Geralmente, os cuidados envolvem jejum de comida e água. Lembre-se: siga tudo com atenção para garantir uma cirurgia em animais no Sacomã mais tranquila. </p>
<p>A anestesia, por sua vez, é um processo delicado dentro da cirurgia em animais no Sacomã. Por possuírem alguns riscos, os veterinários dividem a anestesia em três etapas, no qual, o processo pode variar em sua duração, dependendo do pet e da complexidade do procedimento. </p>
<p>Pois bem, se seu pet passar por uma operação, não há motivos para medo. Seguindo as recomendações indicadas, o pet logo estará recuperado e saudável.</p>
<h2>Faça a cirurgia em animais no Sacomã conosco!</h2>
<p>Há mais de 6 anos atuando neste ramo, nós garantimos soluções a sua necessidade, com valores acessíveis e justos em conjunto com diversas formas de pagamento para facilitar a sua parceria.</p>
<p>Nós garantimos acompanhamento diário em cirurgias, enfermidades, distúrbios e disfunções apresentadas pelas diversas raças e espécies de animais, a experiente equipe obteve a grande oportunidade de compartilhar seus conhecimentos de maneira totalmente independente, formando seu próprio centro de atendimento veterinário. </p>
<p>Entendemos que é possível prestar um atendimento de ótima qualidade sem a necessidade de se cobrar valores muitas vezes inacessíveis a muitos clientes. Por isso, focamos em preços populares sem a mínima perda de qualidade. Deixe os detalhes conosco e desfrute de um trabalho bem feito. Não perca mais tempo e nem a oportunidade de se tornar parceiro de uma clínica que prioriza e respeita a qualidade de vida de seu pet. Entre em contato conosco agora mesmo, tire todas as suas dúvidas com a nossa equipe e saiba mais. Esperamos por você.</p>

                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>