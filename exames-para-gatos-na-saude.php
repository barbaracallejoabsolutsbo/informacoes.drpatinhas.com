<?php
    $title       = "Exames para Gatos na Saúde";
    $description = "O preço dos exames para gatos na Saúde podem variar muito, não apenas de acordo com o laboratório, mas também devido ao que foi solicitado.
";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    <?php // include "includes/modal-orcamento-qsmi.php"; ?>

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php // echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>Em geral, os exames para gatos na Saúde podem ser solicitados pelo médico veterinário tanto para check-up, quanto para auxiliar em um diagnóstico. Sabendo dessa importância, a Dr. Patinhas não hesita em fazer um bom trabalho e conta com profissionais excepcionais para solucionar a sua necessidade, de forma eficiente e mais rápida.</p>

<p>Por vezes, os gatos podem adoecer por diversos fatores, por isso, para uma melhor avaliação, o profissional solicita exames para gatos na Saúde complementares. Mas não se preocupe, a qualquer hora do dia, estamos disponíveis para tirar todas as suas dúvidas.</p>

<h2>Quais são os tipos de exames para gatos na Saúde?</h2>

<p>O hemograma é o mais comum entre os exames para gatos na Saúde solicitados, pois, ele avalia e quantifica as células do sangue do animal, servindo também para outros exames para gatos na Saúde, como dosagens bioquímicas renais e hepáticas, glicemia, colesterol e triglicérides.</p>

<p>De forma resumida, o hemograma é um dos mais importantes exames para gatos na Saúde, pois avalia a série vermelha, responsável principalmente pela oxigenação celular; a série branca, responsável pela defesa do corpo e a contagem de plaquetas, responsável pela coagulação.</p>

<p>É importante lembrar que, nos exames para gatos na Saúde, o jejum alimentar é quase sempre necessário, tendo em vista que assim, o material coletado será analisado pelo laboratório de forma mais eficiente e completa. Com os resultados em mãos, o médico veterinário analisará se há alguma anomalia, e em caso positivo, indicar o tratamento correto ao seu pet.</p>
<h2> </h2>
<h2>Conte com os exames para gatos na Saúde da Dr. Patinhas!</h2>

<p>Aqui, a prioridade é a saúde de seu pet. Pensando nisso, nós visamos agregar valores acessíveis e justos em conjunto com diversas formas de pagamento para facilitar a sua parceria. E ainda, todos os nossos profissionais passam por um treinamento adequado para fornecer o atendimento personalizado que o cliente procura e merece.</p>

<p>Vale frisar que, a nossa equipe acompanha diariamente cirurgias, enfermidades, distúrbios e disfunções apresentadas pelas diversas raças e espécies de animais. Além disso, atendemos também animais exóticos, exceto aves. </p>

<p>Diante de todos esses fatores, salientamos, por fim, que oferecemos vacinas importadas, medicamentos, castração de animais, limpeza de tártaro, piometra e cesárea, além do excelente e cuidadoso tratamento no banho e tosa. Deixe os detalhes com a nossa equipe, tire todas as suas dúvidas e tenha um suporte completo, com toda presteza e atenção necessária. No momento em que entrar em contato conosco, você notará que encontrou a clínica ideal para cuidar de seu pet, seja esteticamente até os procedimentos cirúrgicos. Não perca mais tempo e nem a oportunidade de se tornar parceiro de uma clínica que prioriza e respeita o cliente e seu bichinho de estimação. Ligue agora mesmo e faça um orçamento sem compromisso. Esperamos por você.</p>

                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>