<?php
$title       = "Diagnóstico laboratório Veterinário na Vila Carrão";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>O Diagnóstico laboratório Veterinário na Vila Carrão é de extrema importância para que a saúde dos pets seja mantida, já que muitas das doenças de animais têm um diagnóstico complicado de ser feito, e necessitam de exames laboratoriais para serem confirmadas. Mas não se preocupe, a nossa equipe oferece o suporte completo a sua necessidade, com presteza e atenção. Não deixe de conhecer nossos trabalhos.</p>
<p>A Dr Patinhas, além de ser especializados em Clínica veterinária noturna, Valor Cesária cachorro e gato, Clínica de Estética animal, Exames para Gatos e Oftalmo para cachorros e Gatos, atuando no mercado de Clinica Veterinária com a missão de oferecer sempre o melhor para seus clientes, de forma que seus objetivos sejam alcançados. Além disso, contamos com profissionais competentes visando prestar o melhor atendimento possível em Diagnóstico laboratório Veterinário na Vila Carrão para ser sempre a opção número um de nossos parceiros e clientes.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>