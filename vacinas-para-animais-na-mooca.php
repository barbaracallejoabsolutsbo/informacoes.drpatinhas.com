<?php
    $title       = "Vacinas para animais na Mooca";
    $description = "As vacinas para animais na Mooca devem ser consideradas essenciais para todas as espécies para não transmitir doenças até mesmo para humanos.
";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    <?php // include "includes/modal-orcamento-qsmi.php"; ?>

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php // echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>Procurando por vacinas para animais na Mooca? Bem vindo a Dr. Patinhas, uma clínica sólida e séria que preza pelo bem estar dos pets em todos os aspectos. Por isso, contamos com uma equipe unida e organizada que está sempre atenta às atualizações do ramo para fornecer o que há de melhor e mais moderno.</p>
<p>As nossas vacinas para animais na Mooca são importadas, bem como oferecemos medicamentos, castração de animais, limpeza de tártaro, piometra e cesárea, além do excelente e cuidadoso tratamento no banho e tosa, com toda presteza e atenção que o cliente procura e merece.</p>
<h2>Por que as vacinas para animais na Mooca são importantes?</h2>
<p>Em geral, as vacinas para animais na Mooca são de suma importância para proteger o animal de estimação contra doenças infecciosas, e protegê-los contra os agentes circulantes dessas doenças. Além disso, elas previnem o contágio do animal com agentes das doenças e permite que o pet possa manter a sua saúde em equilíbrio por mais tempo.</p>
<p>Vale lembrar que as vacinas para animais na Mooca devem ser dadas, visto que algumas doenças podem acometer não somente aos pets, mas também aos seres humanos. </p>
<p>As vacinas para animais na Mooca proporcionam uma vida mais longa e saudável para os pets, garantindo uma proteção contra doenças infecciosas, que podem chegar por vírus, bactérias, entre outros. É um critério básico para que os pets possam visitar parques, ir à pet shops, se hospedar em hoteizinhos e creches, ou seja, conviver com segurança nos ambientes.</p>
<p>De forma conclusiva, as vacinas para animais na Mooca são uma forma de proteger a saúde de todos que convivem com eles, principalmente dentro de casa. Consulte um profissional para aumentar a qualidade de vida de seu pet.</p>
<h2>Saiba mais sobre as vacinas para animais na Mooca da Dr. Patinhas:</h2>
<p>Primeiramente, ao entrar em contato conosco, você notará que fez a escolha certa. E ainda, pensando no bem estar completo de todos, nós visamos agregar valores acessíveis e justos em conjunto com diversas formas de pagamento para facilitar a sua contratação.</p>
<p>Vale salientar que também atendemos animais exóticos e possuímos uma experiência de 6 anos de acompanhamento diário em cirurgias, enfermidades, distúrbios e disfunções apresentadas pelas diversas raças e espécies de animais. Está esperando o que para ligar agora mesmo, tirar todas as suas dúvidas e se tornar o nosso mais novo parceiro de longa data? Esperamos por você para realizar um orçamento sem compromisso.</p>
                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>