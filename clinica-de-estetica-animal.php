<?php
    $title       = "Clínica de Estética animal";
    $description = "A Dr. Patinhas é uma clínica de estética animal destaque no ramo, pois, além da qualidade, segue à risca todos os padrões rigorosos de higienização.
";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    <?php // include "includes/modal-orcamento-qsmi.php"; ?>

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php // echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>Conte com a melhor clínica de estética animal chamada Dr. Patinhas. Com anos de experiência no ramo, não hesitamos em fazer um bom trabalho e buscamos superar as expectativas de nossos clientes, sempre mantendo o padrão de qualidade em atendimento aos pets, bem como aos clientes, de forma totalmente personalizada.</p>

<p>A nossa clínica de estética animal se destaca, pois, é confortável, climatizada e higienizada, para o seu pet, com o intuito de deixar o bichinho o mais calmo possível, para o contato com o veterinário na hora da consulta ser tranqüilo. A qualquer hora do dia, estamos disponíveis para fornecer todo o suporte necessário, com presteza e atenção.</p>

<p>Saiba o que uma clínica de estética animal faz:</p>

<p>Em geral, uma clínica de estética animal é uma opção para quem deseja se diferenciar da concorrência e montar um pet shop de sucesso, onde os clientes poderão levar seus amigos de quatro patas para um dia especial com banho e tosa, atendimento veterinário, espaço para atividades físicas, brincadeiras, entre outros benefícios.</p>
<p>São nessas atividades que uma clínica de estética animal se faz útil e cada vez mais procurada, tendo em vista que, a clínica de estética animal  é um estabelecimento onde se pode levar o pet para ter um dia de beleza, um ambiente onde ele possa receber tratamentos como:</p>
<ul>
<li>
<p>Cuidado com a pelagem, entre hidratação e tosa;</p>
</li>
<li>
<p>Um bom banho;</p>
</li>
<li>
<p>Saúde bucal do animal, etc.</p>
</li>
</ul>
<p>Pois bem, a clínica de estética animal que deseja obter sucesso na conquista dos clientes precisa oferecer serviços adicionais como um diferencial. Para isso é preciso enxergar as necessidades do mercado, visto que o primeiro passo de um empreendedor deve ser a pesquisa de mercado. Por isso, analisar primeiramente o que seus concorrentes fazem é fundamental para quem busca inovar na área.</p>
<p>Dr. Patinhas – A melhor clínica de estética animal </p>
<p>Em caso de emergências, nós possuímos uma completa infraestrutura que permite atendimento rápido em casos de urgência, além da equipe de médicos veterinários amplamente treinada para este tipo de situação. Além disso, nós visamos agregar valores acessíveis e justos em conjunto com diversas formas de pagamento para facilitar a sua parceria.</p>
<p>Vale frisar que, nós oferecemos vacinas importadas, medicamentos, castração de animais, limpeza de tártaro, piometra e cesárea, além do excelente e cuidadoso tratamento no banho e tosa. Deixe os detalhes conosco e desfrute de um trabalho bem feito. Entre em contato agora mesmo e faça um orçamento.</p>

                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>