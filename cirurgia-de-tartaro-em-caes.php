<?php
    $title       = "Cirurgia de tártaro em cães";
    $description = "Para uma completa e eficiente cirurgia de tártaro em cães é aplicada anestesia geral durante a remoção do tártaro, de modo que o bichinho não é prejudicado.";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    <?php // include "includes/modal-orcamento-qsmi.php"; ?>

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php // echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>De forma resumida, a cirurgia de tártaro em cães é a especialidade médica que realiza procedimentos invasivos com finalidade terapêutica, e diagnóstica. Neste sentido, a Dr. Patinhas, com anos de experiência no ramo, realiza também cirurgias eletivas, emergenciais e de alta complexidade. E ainda, oferecemos todo o atendimento personalizado que o cliente e seu pet, procuram e merecem.</p>

<p>Em suma, a cirurgia de tártaro em cães se resume na remoção da placa bacteriana com um aparelho chamado ultrassom odontológico. Lembrando que para limpar os tártaros dos cães, eles precisam estar anestesiados.</p>
<h2>Conhecendo mais informações da cirurgia de tártaro em cães:</h2>
<p>O tártaro é um problema frequente quando se trata da saúde bucal de animais domésticos, sobretudo cães e gatos. Para evitar maiores problemas, é necessário garantir a higiene bucal do seu pet e evitar cirurgia de tártaro em cães.</p>
<p>Durante a cirurgia de tártaro em cães de remoção, a anestesia ajuda muito o seu amigo a não passar por um trauma  A anestesia durante a cirurgia de tártaro em cães  nos pets garante um procedimento bem feito em todas as faces do dente, inclusive nas áreas do palato e da língua, bem como o correto polimento dos dentes para retardar novos acúmulos. </p>
<p>Lembrando que, fazer a cirurgia de tártaro em cães com o animal acordado e se mexendo não garante a mesmo resultado. Além disso, gera stress e desconforto ao pet. </p>
<p>É difícil realizar uma boa limpeza de tártaro se o médico veterinário não consegue mexer em todos os dentes do animal, dos incisivos aos molares pequenos e mais profundos. Consulte um profissional.</p>
<h2>Cirurgia de tártaro em cães é com a Dr. Patinhas!</h2>
<p>Venha conhecer o nosso consultório e ter a certeza de que encontrou a clínica ideal para se tornar o mais novo parceiro de longa data. O nosso consultório é confortável, climatizado e higienizado, para o seu pet. A ideia é deixar o bichinho o mais calmo possível, para o contato com o veterinário na hora da consulta ser tranquilo.</p>
<p>Além disso, visamos agregar valores acessíveis e justos em conjunto com diversas formas de pagamento para facilitar a sua parceria. E ainda, nós possuímos uma completa infra-estrutura que permite atendimento rápido em casos de urgência, além da equipe de médicos veterinários amplamente treinada para este tipo de situação.</p>
<p>Se interessou? Deixe os detalhes conosco e desfrute de um trabalho bem feito. Ligue agora mesmo, tire todas as suas dúvidas e faça um orçamento sem compromisso. Esperamos por você.</p>

                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>