<?php
$title       = "Diagnóstico laboratório Veterinário em Socorro";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Apesar de essencial e de extrema importância para o sucesso do Diagnóstico laboratório Veterinário em Socorro, a experiência do veterinário não é suficiente para entender, por exemplo, quais nutrientes estão em deficiência no animal. Dessa forma, com os exames laboratoriais, é possível a realização de diagnósticos mais completos. Em nossa clínica nós temos todos os recursos necessário para todos os exames.</p>
<p>Procurando por uma empresa de confiança onde você tenha profissionais qualificados com o intuito de suprir suas necessidades. A empresa Dr Patinhas ganha destaque quando o assunto é Clinica Veterinária. Ainda, contamos com uma equipe qualificada para atender suas necessidades, seja quando se trata de Diagnóstico laboratório Veterinário em Socorro até Ultrassom em cachorro, Hospital para Cachorro e gato, Exames para animais, Tosa em cães e Castração de Gato com muita excelência.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>