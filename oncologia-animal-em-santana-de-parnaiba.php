<?php
$title       = "Oncologia Animal em Santana de Parnaíba";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Está procurando por Oncologia Animal em Santana de Parnaíba? Bem vindo a Dr. Patinhas. Após 6 anos atuando neste ramo, nós garantimos um atendimento personalizado ao cliente, bem como soluções completas e modernas, pois  estamos sempre atualizados do mercado para fornecer serviços de última geração. A área responsável por diagnosticar câncer em pequenos animais, sendo uma especialidade que ganhou grande destaque.</p>
<p>Buscando por uma empresa de credibilidade no segmento de Clinica Veterinária, para que, você que busca por Oncologia Animal em Santana de Parnaíba, tenha a garantia de qualidade e idoneidade, contar com a Dr Patinhas é a opção certa. Aqui tudo é feito por um time de profissionais com amplo conhecimento em Dermatologista de Cachorro, Exames para animais, Atendimento para animais, Valor Cesária cachorro e gato e Ultrassom em cachorro para assim, oferecer a todos os clientes as melhores soluções.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>