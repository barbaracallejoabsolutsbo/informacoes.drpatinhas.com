<?php
    $title       = "Acupuntura para animais no Sacomã";
    $description = "Em casos crônicos os animais podem precisar de sessões esporádicas de acupuntura para animais no Sacomã para a manutenção adequada do tratamento.";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    <?php // include "includes/modal-orcamento-qsmi.php"; ?>

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php // echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>Está procurando por acupuntura para animais no Sacomã? Pois, você está no lugar certo. A equipe Dr Patinhas é especializada há mais de 6 anos neste ramo, garantindo acompanhamento diário em todos os procedimentos fornecidos a diversas raças e espécies de animais.</p>

<p>Em geral, quem conhece e experimenta a nossa acupuntura para animais no Sacomã pode confirmar a excelência desde o atendimento personalizado até o serviço completo. A qualquer hora do dia, nós estamos disponíveis para tirar todas as suas dúvidas e fornecer todo o suporte necessário, com presteza e atenção.</p>

<h2>Saiba mais sobre acupuntura para animais no Sacomã:</h2>

<p>A acupuntura para animais no Sacomã segue a mesma linha de objetivos traçados pela acupuntura em humanos, que é resgatar o máximo da qualidade de vida do pet. Atualmente, a acupuntura para animais no Sacomã é muito procurada e é um dos principais métodos terapêuticos na busca por um equilíbrio maior do corpo. </p>
<p>De forma resumida, a acupuntura para animais no Sacomã tem o objetivo de ajudar o corpo a se curar, tendo em vista que a mesma age melhorando a circulação sanguínea, estimulando o funcionamento do sistema nervoso e promovendo a liberação de substâncias analgésicas e anti-inflamatórias pelo organismo.</p>
<p>É importante frisar que, a acupuntura para animais no Sacomã também pode atuar como um tratamento complementar em doenças crônicas, como câncer, insuficiência renal, tratamentos de problemas de pele e doenças auto imunes, geriatria, problemas endócrinos e diversos outros, ajudando a reduzir o uso de medicações e a trazer qualidade de vida.</p>
<p>Lembrando que, a acupuntura não causa dor e não possui efeitos colaterais, no entanto, pode causar sonolência e relaxamento do animal, que por muitas vezes é até o desejado e esperado.</p>
<h2>Dr. Patinhas - Acupuntura para animais no Sacomã de qualidade</h2>
<p>Pois bem, o nosso consultório é confortável, climatizado e higienizado, com o intuito de deixar o pet relaxado e a consulta veterinária, bem como os procedimentos a serem realizados, sejam mais suaves e tranqüilos. Além disso, vale salientar que, nós visamos agregar valores acessíveis e justos em conjunto com diversas formas de pagamento para facilitar a sua parceria conosco.</p>
<p>A nossa clínica conta com uma equipe especializada para cuidar de todos os detalhes do tratamento de seu pet, seja do estético até o cirúrgico. Por fim, vale frisar que, possuímos uma completa infraestrutura que permite atendimento rápido em casos de urgência. Deixe os detalhes conosco e desfrute de um trabalho bem feito. Ligue agora mesmo e saiba mais.</p>

                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>