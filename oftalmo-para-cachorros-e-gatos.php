<?php
    $title       = "Oftalmo para cachorros e Gatos";
    $description = "Verifique a disponibilidade de consulta com o oftalmo para cachorros e gatos na clínica Dr. Patinhas e tenha soluções rápidas e eficientes a sua necessidade.
";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    <?php // include "includes/modal-orcamento-qsmi.php"; ?>

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php // echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p> oftalmo para cachorros e gatos, em geral, serve para tratar as doenças oculares em cães, gatos e outros animais, realizando os exames e dando os diagnósticos necessários. Sabendo dessa importância, a Dr. Patinhas é uma clínica séria que não hesita em fazer um bom trabalho e conta com uma equipe qualificada para atender a necessidade do cliente, de forma rápida e eficiente.</p>

<p>É muito comum que os pets passem por um clínico geral antes de serem encaminhados para um oftalmo para cachorros e gatos. Ao longo dos anos, diversas doenças oculares podem aparecer e por isso, é importante que você sempre esteja atento ao comportamento de seu pet.</p>

<h2>Mais sobre oftalmo para cachorros e gatos:</h2>

<p>É importante frisar que, em casos de conjuntivite e de córnea não complicada, o clínico geral pode tratar. No entanto, é de suma importância que nos demais casos, o oftalmo para cachorros e gatos seja responsável pelo tratamento.</p>

<p>Conforme supracitado, é o clínico geral que encaminhará o pet para o oftalmo para cachorros e gatos. No entanto, em alguns casos específicos, a consulta pode ser diretamente agendada com o profissional oftalmo para cachorros e gatos. Confira abaixo alguns destes casos:</p>

<ul>
<li>
<p>Secreção ocular;</p>
</li>
<li>
<p>Olhos vermelhos;</p>
</li>
<li>
<p>Sensibilidade a luz;</p>
</li>
<li>
<p>Lacrimejamento intenso, entre outros.</p>
</li>
</ul>

<p>Se você está em dúvidas se o seu pet necessita de um oftalmo para cachorros e gatos, pesquise clínicas especializadas no assunto que indicarão a melhor solução ao seu bichinho.</p>

<h2>Conheça o oftalmo para cachorros e gatos da Dr. Patinhas</h2>

<p>Em primeiro lugar, o nosso consultório é confortável, climatizado e higienizado, para o seu pet. A idéia é deixar o bichinho o mais calmo possível, para o contato com o veterinário na hora da consulta ser tranqüilo.</p>
<p>Além disso, todos os nossos profissionais são qualificados para diversos momentos, inclusive no atendimento rápido em casos de urgência, além da equipe de médicos veterinários amplamente treinada para este tipo de situação.</p>
<p>Diante de todos esses fatores, lembramos que prezamos pelo bem estar do pet como se fosse uma pessoa mesmo, por isso, para cuidar melhor deles, nós agregamos valores acessíveis e justos em conjunto com diversas formas de pagamento para facilitar a sua parceria e contratação. Mas não se preocupe, pois, caso haja dúvidas, a qualquer hora do dia, nós estamos disponíveis para fornecer todo o suporte necessário, com presteza e atenção. Se interessou? Ligue agora mesmo e faça um orçamento totalmente sem compromisso. Esperamos por você.</p>

                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>