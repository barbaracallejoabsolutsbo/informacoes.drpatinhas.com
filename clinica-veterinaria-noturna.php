<?php
    $title       = "Clínica veterinária noturna";
    $description = "Lembre-se: Verifique as referências antes de buscar uma clínica veterinária noturna, pois assim, você poderá ter a certeza de oferecer o melhor ao seu pet.
";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    <?php // include "includes/modal-orcamento-qsmi.php"; ?>

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php // echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>Para montar uma clínica veterinária noturna é importante ter uma estrutura de proteção tanto para você quanto para quem está indo consultar na sua clínica. Afinal, além de acessível, é preciso que a entrada e o interior da loja sejam acolhedores, de preferência com referências para casos de emergência, como por exemplo: lojas próximas.</p>
<p>Neste sentido, a Dr. Patinhas se destaca como clínica veterinária noturna, pois, além de segurança completa, nós oferecemos um atendimento personalizado aos clientes e pets, bem como soluções completas e rápidas. Além disso, a qualquer hora do dia, estamos disponíveis para fornecer todo o suporte necessário, com presteza e atenção.</p>
<h2>Saiba mais sobre clínica veterinária noturna:</h2>
<p>Pois bem, para montar uma clínica veterinária noturna deve-se atender a diversos requisitos. Vamos conhecer alguns:</p>
<ul>
<li>
<p>Infraestrutura: Primeiro tenha um estudo da região em que pretende ter um negócio 24 horas. </p>
</li>
<li>
<p>Demanda: conheça o seu público. Realize uma pesquisa de mercado para saber se vai ter clientes para atender no período integral e noturno. </p>
</li>
<li>
<p>Comunidade: Avalie negócios próximos ao local da sua clínica. Se você tiver a oportunidade de fazer parcerias no entorno, isso pode gerar um ambiente colaborativo. </p>
</li>
</ul>
<p>Lembrando que, a clínica veterinária noturna varia de acordo com o foco do empreendimento, pois, o seu negócio pode ser voltado para especialidades específicas, serviços gerais ou os dois.</p>
<p>Uma clínica veterinária noturna com internação vai demandar que tenha pessoas responsáveis por controle de urgências. Além do cuidado com controle de horários de medicações, dosagens, é importante ter o controle nutricional e dos leitos dos pets.</p>
<h2>Vantagens da nossa clínica veterinária noturna:</h2>
<p>Primeiramente, para melhor atender as necessidades de nossos pacientes, contamos com excelentes colaboradores que ao serem requisitados, prontamente se deslocam à nossa clínica para efetuarem os exames necessários.</p>
<p>Além disso, como destaque no ramo de clínica veterinária noturna, nós agregamos o melhor custo benefício do mercado em conjunto com diversas formas de pagamento para facilitar a sua parceria. </p>
<p>De modo geral, nós possuímos uma completa infraestrutura que permite atendimento rápido em casos de urgência, além da equipe de médicos veterinários amplamente treinada para este tipo de situação. E ainda, o nosso consultório é confortável, climatizado e higienizado, para o seu pet. A ideia é deixar o bichinho o mais calmo possível, para o contato com o veterinário na hora da consulta ser tranqüilo. Ligue agora mesmo, tire todas as suas dúvidas com a nossa equipe e tenha certeza de que fez a escolha certa. </p>

                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>