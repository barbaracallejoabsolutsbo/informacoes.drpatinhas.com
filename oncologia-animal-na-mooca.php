<?php
    $title       = "Oncologia Animal na Mooca";
    $description = "A oncologia animal na Mooca nada mais é do que um estudo de tumores, que também é conhecido como cancerologia. Venha saber mais com os nossos profissionais.
";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    <?php // include "includes/modal-orcamento-qsmi.php"; ?>

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php // echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>Está procurando por oncologia animal na Mooca? Bem vindo a Dr. Patinhas. Após 6 anos atuando neste ramo, nós garantimos um atendimento personalizado ao cliente, bem como soluções completas e modernas, tendo em vista que estamos sempre atentos às atualizações do mercado para fornecer serviços de última geração.</p>

<p>Pois bem, a oncologia animal na Mooca é a área responsável por diagnosticar e tratar câncer em pequenos animais, sendo uma especialidade que ganhou grande destaque devido ao resultado da vida mais longa dos pets que sofrem com este problema.</p>

<h2>Como funciona a oncologia animal na Mooca?</h2>

<p>A oncologia animal na Mooca  tem se despontado como a principal esperança para os tutores, pois, além da idade, existem outros fatores externos que podem levar ao desenvolvimento de câncer nos pequenos animais. </p>
<p>O câncer, por exemplo, é uma das principais causas de morte em cães e gatos, por isso, o profissional que deseja atuar na prevenção, diagnóstico e tratamento dessa doença, precisa se especializar em oncologia animal na Mooca para identificar o tipo, a causa exata e o tratamento mais indicado em cada caso.</p>
<p>A melhor estratégia do especialista em oncologia animal na Mooca deve ser realizar os exames corretos para detectar com precisão a enfermidade. Principalmente, pois, quando diagnosticada precocemente, as chances do tratamento resultar em cura são bem maiores.</p>
<p>Na oncologia animal na Mooca há alguns tratamentos indicados, como por exemplo:</p>
<ul>
<li>
<p>Quimioterapia;</p>
</li>
<li>
<p>Radioterapia;</p>
</li>
<li>
<p>Imunoterapia; e </p>
</li>
<li>
<p>Eletroquimioterapia.</p>
</li>
</ul>
<p>Lembrando que, neste ramo, é fundamental conhecer as origens do problema e, principalmente, as formas de diagnóstico para que a doença seja diagnosticada com precisão o mais breve possível.</p>
<h2>Oncologia animal na Mooca é com Dr. Patinhas!</h2>
<p>Nós possuímos uma completa infraestrutura que permite atendimento rápido em casos de urgência, além da equipe de médicos veterinários amplamente treinada para este tipo de situação. Para melhor atender as necessidades de nossos pacientes, contamos com excelentes colaboradores que ao serem requisitados, prontamente se deslocam à nossa clínica para efetuarem os exames necessários. Além disso, agregamos o melhor custo benefício do mercado em conjunto com diversas formas de pagamento para facilitar a sua parceria.</p>
<p>Por fim, o nosso consultório é confortável, climatizado e higienizado, para o seu pet. Deixe os detalhes com a nossa equipe e desfrute de um trabalho bem feito. No momento em que entrar em contato conosco, você notará que fez a escolha certa. Ligue agora mesmo, tire as suas dúvidas e faça um orçamento sem compromisso. Esperamos por você.</p>

                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>