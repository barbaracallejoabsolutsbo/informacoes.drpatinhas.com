<?php
    $title       = "Banho para cachorro no Ipiranga";
    $description = "Atualmente, o banho para cachorro no Ipiranga pode ser a base de própolis verde, recomendado tanto para gatos quanto para cachorros. Venha saber mais.
";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    <?php // include "includes/modal-orcamento-qsmi.php"; ?>

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php // echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>Procurando por banho para cachorro no Ipiranga? Conte com a equipe Dr. Patinhas e tenha soluções completas. Há mais de 6 anos atuando neste ramo, não hesitamos em fazer um bom trabalho e oferecemos com vacinas importadas, medicamentos, castração de animais, limpeza de tártaro, piometra e cesárea, além do excelente e cuidadoso tratamento no banho e tosa.</p>

<p>Em geral, a demanda em busca do nosso banho para cachorro no Ipiranga vêm crescendo mais a cada dia que passa, pois, oferecemos um atendimento personalizado a cada cliente, bem como atendemos todos os tipos de animais, inclusive os exóticos. A qualquer hora do dia, esperamos por seu contato para tirar as suas dúvidas.</p>

<h2>Mais informações sobre o banho para cachorro no Ipiranga:</h2>
<h2> </h2>
<p>A higiene e os cuidados de banho para cachorro no Ipiranga é dever do dono, pois o bichinho de estimação é dependente do seu dono e merece se sentir bem. </p>
<p>O banho para cachorro no Ipiranga em filhotes: O banho em pet shop só é válido após seu cão ter sido vacinado e vermifugado, desse modo o primeiro banho dele será em casa. Alguns veterinários podem indicar banho, mesmo em casa, apenas após o término das vacinas e vermifugação.</p>
<p>É importante salientar que também existe o banho para cachorro no Ipiranga estético, no qual tem como finalidade manter seu cachorro limpo e cheiroso, também realizado antes do procedimento de tosa, existe o banho para cachorro no Ipiranga conhecido como terapêutico. </p>
<p>De forma conclusiva, no banho estético o shampoo utilizado é simples e voltado para uso em cães, sem qualquer princípio ativo, enquanto no banho terapêutico é utilizado um shampoo indicado por um veterinário para tratar uma dermopatia, por exemplo.</p>
<h2>Realize o banho para cachorro no Ipiranga com a Dr. Patinhas!</h2>
<p>Primeiramente, é válido ressaltar que, nós possuímos uma completa infra-estrutura que permite atendimento rápido em casos de urgência, além da equipe de médicos veterinários amplamente treinada para este tipo de situação.</p>
<p>Além disso, nós visamos agregar valores acessíveis e justos em conjunto com diversas formas de pagamento para facilitar a sua parceria. E ainda, desde o primeiro contato, nós estabelecemos uma relação de transparência e comprometimento para que ambas as partes se sintam confortáveis e seguras.</p>
<p>Por fim, ressaltamos que o nosso consultório é confortável, climatizado e higienizado, para o seu pet. A ideia é deixar o bichinho o mais calmo possível, para o contato com o veterinário na hora da consulta ser tranquilo. Ligue agora mesmo e faça um orçamento sem compromisso.</p>

                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>