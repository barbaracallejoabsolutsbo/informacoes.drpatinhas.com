<?php
    $title       = "Banho e tosa para animais";
    $description = "A higiene e os cuidados de banho e tosa para animais são deveres do dono, pois o bichinho de estimação é dependente do seu dono e merece cuidados.
";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    <?php // include "includes/modal-orcamento-qsmi.php"; ?>

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php // echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>Em suma, o banho e tosa para animais é realizado por um profissional responsável por cuidar da beleza e do bem estar completo do pet, pois, ele que dá banho, corta e apara os pelos do animal, corta as unhas, enfim, cuida dos processos de higienização do bichinho.</p>

<p>Sabendo dessa importância, o Dr. Patinhas não hesita em fazer um bom trabalho e conta com profissionais especializados para fornecer o atendimento personalizado ao cliente, bem como um banho e tosa para animais inigualável aos demais. Não se preocupe, a qualquer hora do dia, nós estamos disponíveis para tirar todas as suas dúvidas.</p>
<h2>Importância do banho e tosa para animais:</h2>
<p>Pois bem, o banho e tosa para animais nada mais são do que ferramentas importantes para manter a saúde do cãozinho e evitar problemas de saúde tanto do animal, quanto do seu dono. </p>
<p>O banho e tosa para animais  auxilia na desinfecção de pulgas, carrapatos e todo tipo de bactéria ou doença de pelos e pele podem ser facilmente evitadas para que o dono não precise gastar mais tendo que tratá-las.</p>
<p>É importante ressaltar que o profissional responsável pelo banho e tosa para animais deve se ater aos detalhes, pois, cada cão tem um tipo de pelagem e um tipo de necessidade diferentes. Vejamos alguns exemplos:</p>
<ul>
<li>
<p>Cães de pelo longo necessitam de escovação regular para não causar nós. A tosa também é necessária em épocas mais quentes;</p>
</li>
<li>
<p>Há cães que possuem dupla pelagem que, além dos pelos comuns, também possuem subpelos e requer cuidados especiais, entre outros.</p>
</li>
</ul>
<p>Lembrando que, alguns cães possuem uma tendência maior a desenvolverem problemas de pele, nesse caso, o banho e tosa para animais  são extremamente necessários para evitar que esses problemas se desenvolvam e causem futuros problemas maiores.</p>
<h2>O melhor banho e tosa para animais é na Dr. Patinhas!</h2>
<p>Oferecemos o melhor custo benefício do mercado em conjunto com diversas formas de pagamento para facilitar a sua parceria. Além disso, desde o primeiro contato com o cliente, é estabelecida uma relação de transparência e comprometimento para que ambas as partes se sintam confortáveis e seguras neste serviço.</p>
<p>Ressaltamos ainda que, para melhor atender as necessidades de nossos pacientes, contamos com excelentes colaboradores que ao serem requisitados, prontamente se deslocam à nossa clínica para efetuarem os exames necessários. Venha conferir. Ligue agora mesmo, tire todas as suas dúvidas, tenha todo o suporte necessário e realize um orçamento sem compromisso.</p>

                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>