<?php
$title       = "Dermatologista de Cachorro no Glicério";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Os deveres habituais de um Dermatologista de Cachorro no Glicério na Saúde incluem avaliar o animal antes do tratamento, realizando uma inspeção completa. Durante a consulta, também é feita uma entrevista com o tutor, para maiores informações coletadas durante o exame clínico. Na maioria dos casos, eles são os principais responsáveis pelas doenças de pele. Entre em contato conosco para saber mais.</p>
<p>Além de sermos uma empresa especializada em Dermatologista de Cachorro no Glicério disponibilizamos uma equipe de profissionais altamente competente a fim de prestar um ótimo atendimento em Clínica veterinária de Domingo, Internação para Gatos, Exames para Gatos, Laboratório para animais e Hospital para cães e gatos. Com a ampla experiência que a equipe Dr Patinhas possui na atualidade, garantimos um constante desenvolvimento voltado a melhorar ainda mais nos destacados entre as principais empresas do mercado de Clinica Veterinária.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>