<?php
    $title       = "Melhor pet shop para banho e tosa";
    $description = "O melhor pet shop para banho e tosa dado por um profissional treinado serve, também, para identificar qualquer anormalidade que esteja no corpo do pet.
";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    <?php // include "includes/modal-orcamento-qsmi.php"; ?>

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php // echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>Atualmente, o melhor pet shop para banho e tosa é a Dr. Patinhas. Há mais de 6 anos atuando neste ramo, não hesitamos em fazer um bom trabalho e estamos sempre atentos às atualizações do mercado para fornecer serviços de última geração, bem como um atendimento sofisticado e personalizado a sua necessidade.</p>

<p>Como melhor pet shop para banho e tosa, a nossa clínica conta com uma equipe treinada e preparada para cuidar da estética do seu pet com todo amor. Além disso, vale salientar que, atendemos até os animais exóticos, exceto aves. </p>

<h2>Principais detalhes do melhor pet shop para banho e tosa:</h2>

<p>Além de produtos para banho e tosa adequados para o seu pet será necessário ter um ambiente que proporcione segurança e conforto ao animal durante o procedimento. Por isso, procure pelo melhor pet shop para banho e tosa.</p>
<p>Abaixo, separamos em tópicos uma lista com algumas vantagens de optar por melhor pet shop para banho e tosa para o seu pet. Confira:</p>
<ul>
<li>
<p>Local adequado: Como o espaço é feito especialmente para isso, tudo será mais tranqüilo e arrumado para que o bichinho tome banho em um local seguro;</p>
</li>
<li>
<p>Produtos de qualidade: Quando o dono opta pelo melhor pet shop para banho e tosa, não precisa se preocupar em adquirir itens específicos para o bichinho;</p>
</li>
<li>
<p>Corte de unhas: O profissional do melhor pet shop para banho e tosa faz esse procedimento com calma e segurança;</p>
</li>
<li>
<p>Limpeza de orelha: Neste ponto de higiene, é de suma importância que o profissional seja qualificado para realizar tal, tendo em vista que é um procedimento delicado, entre outros.</p>
</li>
</ul>
<p>Por fim, além do banho, é válido que nesse ambiente já seja possível fazer a tosa higiênica ou total, de acordo com a preferência do dono do pet.</p>
<h2>Dr. Patinhas – O melhor pet shop para banho e tosa!</h2>
<p>Dentre os nossos diversos serviços, podemos citar que trabalhamos com vacinas importadas, medicamentos, castração de animais, limpeza de tártaro, piometra e cesárea, além do excelente e cuidadoso tratamento no banho e tosa. Além disso, prezando pelo bem estar completo do cliente, nós visamos agregar valores acessíveis e justos em conjunto com diversas formas de pagamento para facilitar a sua parceria.</p>
<p>Diante de todos esses fatores, destacamos que nós possuímos uma completa infra-estrutura que permite atendimento rápido em casos de urgência, além da equipe de médicos veterinários amplamente treinada para este tipo de situação. Deixe os detalhes conosco e ligue agora mesmo para saber mais.</p>

                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>