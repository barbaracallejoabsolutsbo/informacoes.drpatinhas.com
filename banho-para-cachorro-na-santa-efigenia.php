<?php
$title       = "Banho para cachorro na Santa Efigênia";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Procurando por Banho para cachorro na Santa Efigênia? Conte com a equipe Dr. Patinhas e tenha soluções completas. Há mais de 6 anos atuando neste ramo, não hesitamos em fazer um bom trabalho e oferecemos com vacinas importadas, medicamentos, castração de animais, limpeza de tártaro, piometra e cesárea, além do excelente e cuidadoso tratamento no banho e tosa. Entre em contato conosco.</p>
<p>Além de sermos uma empresa especializada em Banho para cachorro na Santa Efigênia disponibilizamos uma equipe de profissionais altamente competente a fim de prestar um ótimo atendimento em Clínica de Estética animal, Banho para cachorro, Acupuntura para animais, Exames para Gatos e Banho para cães e Gatos. Com a ampla experiência que a equipe Dr Patinhas possui na atualidade, garantimos um constante desenvolvimento voltado a melhorar ainda mais nos destacados entre as principais empresas do mercado de Clinica Veterinária.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>