<?php
$title       = "Vacina contra Raiva em Bonsucesso - Guarulhos";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>A raiva é uma doença facilmente contraída, principalmente, por animais que vivem em locais com diversos exemplares silvestres. Por isso, a Vacina contra Raiva em Bonsucesso - Guarulhos é importante, tendo em vista que esta doença deixa os animais vulneráveis. Trata-se de uma infecção viral aguda que pode acometer animais e seres humanos, que contraem a doença por uma mordida causada por animais infectados.</p>
<p>Se você está em busca de Vacina contra Raiva em Bonsucesso - Guarulhos tem preferência por uma empresa com conhecimento e custo-benefício, a Dr Patinhas é a melhor opção para você. Com uma equipe formada por profissionais amplamente qualificados e dedicados para oferecer soluções personalizadas para cada cliente que busca pela excelência. Entre em contato com a gente e saiba mais sobre Clinica Veterinária e todos os nossos serviços como Emergência para animais, Hospital para cães e gatos, Exames para Cachorro, Produtos Hydra para seu animal e Oftalmologia para animais.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>