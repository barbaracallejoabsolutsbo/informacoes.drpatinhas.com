<?php
    $title       = "Hospital veterinário em São Paulo";
    $description = "Venha conhecer toda a estrutura que oferecemos em nosso hospital veterinário em São Paulo e tenha a certeza que encontrou a solução completa.
";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    <?php // include "includes/modal-orcamento-qsmi.php"; ?>

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php // echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>Procurando por um hospital veterinário em São Paulo? Conte com a Dr. Patinhas para solucionar o seu problema. Somos uma clínica sólida e séria que possui uma vasta experiência no ramo, sempre mantendo o nível de qualidade desde o início até o serviço completo.</p>

<p>Como hospital veterinário em São Paulo, nós estabelecemos desde o início, uma relação de transparência e comprometimento para que ambas as partes se sintam mais confortáveis e seguras na relação. E ainda, a qualquer hora do dia, nós estamos disponíveis para fornecer todo o suporte necessário, com presteza e atenção.</p>

<h2>O que faz um hospital veterinário em São Paulo?</h2>

<p>Em geral, um hospital veterinário em São Paulo é um ambiente com mais estrutura e setores variados, atendendo mais animais, devido ao volume maior de profissionais disponíveis.</p>

<p>O ambiente ambulatório do hospital veterinário em São Paulo é um dos setores mais importantes, tendo em vista que está apto a receber animais de médio e grande porte para uma avaliação inicial e uma triagem.</p>

<p>De forma resumida, o hospital veterinário em São Paulo é uma instituição mista ou pública que cobra por alguns serviços ou exige cobertura médica particular em alguns casos.</p>

<p>É importante frisar que o hospital veterinário em São Paulo tem uma abrangência maior do que as demais. Dentre as suas obrigatoriedades, podemos incluir:</p>

<ul>
<li>
<p>área de diagnóstico (laboratório de análises clínicas, radiologia e ultrassonografia – no mínimo); </p>
</li>
<li>
<p>departamento cirúrgico – com estrutura de urgência;<br />internação – com acomodações de isolamento e estrutura de </p>
</li>
<li>
<p>higienização,;</p>
</li>
<li>
<p>setor de sustentação, entre outros.</p>
</li>
</ul>
<p>Geralmente, o horário de funcionamento é de 24 horas para internação e atendimento de emergência.</p>
<h2>Venha conhecer o melhor hospital veterinário em São Paulo!</h2>
<p>Conosco, você tem a tranquilidade de receber serviços de qualidade, através de profissionais qualificados que garantem toda a segurança ao bichinho de estimação. Além disso, agregamos o melhor custo benefício do mercado em conjunto com diversas formas de pagamento para facilitar a sua parceria.</p>
<p>Por fim, nós possuímos uma infraestrutura que permite atendimento rápido em casos de urgência, além da equipe de médicos veterinários amplamente treinada para este tipo de situação. A nossa clínica conta com uma equipe treinada e preparada para cuidar da estética do seu pet com todo amor. Deixe os detalhes conosco e desfrute de um trabalho bem feito. No momento em que entrar em contato conosco, você notará que fez a escolha ideal. Ligue agora mesmo e tire todas as suas dúvidas. Esperamos por você.</p>

                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>