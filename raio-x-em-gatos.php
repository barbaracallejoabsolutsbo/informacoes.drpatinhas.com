<?php
    $title       = "Raio X em gatos";
    $description = "Caso seja necessária uma intervenção cirúrgica, a precisão do raio X em gatos será fundamental para o sucesso do procedimento em seu pet.
";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    <?php // include "includes/modal-orcamento-qsmi.php"; ?>

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php // echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>No momento em que entrou em contato com a Dr. Patinhas, você notará que encontrou o melhor raio X em gatos. Com anos de experiência no ramo, não hesitamos em fazer um bom trabalho e estamos sempre atentos às atualizações do mercado para fornecer o que há de melhor e mais moderno aos clientes.</p>

<p>O raio X em gatos se destina ao diagnóstico de doenças e outras anormalidades em animais, através de exames de imagem, com ou sem a aplicação de radiação ionizante. Além disso, a qualquer hora do dia, nós estamos disponíveis para tirar todas as suas dúvidas e fornecer todo o suporte necessário.</p>

<h2>Conhecendo melhor o raio X em gatos:</h2>

<p>Os casos mais comuns em que os médicos veterinários pedem exames de raio X em gatos são para examinar tecidos ósseos e articulações, alterações nas regiões torácica e abdominal, nos casos de ingestão de objetos estranhos s e também em caso de tumores.</p>
<p>Em geral, há dois tipos de raio X em gatos, o contratado que é um procedimento mais complexo para os animais, que são injetados nos gatos. Apesar de ser um procedimento seguro, ele é mais demorado. No entanto, ele apresenta resultados mais detalhados e específicos. Por isso, para realização deste tipo de raio X em gatos deve-se realizar a limpeza intestinal do animal, fazer jejum e retirar objetos metálicos como coleiras. </p>
<p>Em contrapartida, o raio X em gatos digital, por sua vez, é o meio mais prático e fácil, pois não irrita o pet, sendo realizado através de uma máquina que lança raios que passam por meio do paciente, gerando dados imediatos para análise. </p>
<h2>Faça raio X em gatos com a Dr. Patinhas!</h2>
<p>Nós possuímos equipamentos modernos de última geração que nos permitem radiografar pets de grande porte com imagens de alta qualidade. De forma resumida, para problemas mais comuns, o laudo fica disponível em até 12 horas, mas, em questões urgentes fica pronto em até 3 horas. </p>
<p>Diante de todos esses fatores, ressaltamos que visamos agregar valores acessíveis e justos em conjunto com diversas formas de pagamento para facilitar a sua parceria. Além disso, desde o primeiro contato, nós estabelecemos uma relação de transparência e comprometimento para que todas as partes se sintam confortáveis neste momento de exames.</p>
<p>Para melhor atender as necessidades de nossos pacientes, contamos com excelentes colaboradores que ao serem requisitados, prontamente se deslocam à nossa clínica para efetuarem os exames necessários. Não perca mais tempo, ligue agora mesmo e saiba mais.</p>

                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>