<?php
$title       = "Oncologia em cães e Gatos na Vila Gustavo";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Existe ainda o chamado tratamento paliativo, indicado para casos crônicos da Oncologia em cães e Gatos na Vila Gustavo, em que o tumor está avançado. Neste sentido, não há possibilidade de cura e os cuidados são apenas para aliviar os sofrimentos. Por fim, é imprescindível que o profissional conheça as origens do problema e as formas de diagnóstico para que a doença seja diagnosticada com precisão.</p>
<p>Especialista no mercado, a Dr Patinhas é uma empresa que ganha visibilidade quando se trata de Oncologia em cães e Gatos na Vila Gustavo, já que possui mão de obra especializada em Dermatologista de Cachorro, Oftalmo para cachorros e Gatos, Melhor pet shop para banho e tosa, Ultrassom em cachorro e Vacina contra Raiva. Nossa empresa vem crescendo e garantindo seu espaço entre as principais empresas do ramo de Clinica Veterinária, onde tem o foco em trazer o que se tem de melhor para seus clientes.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>