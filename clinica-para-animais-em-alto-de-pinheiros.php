<?php
$title       = "Clínica para Animais em Alto de Pinheiros";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Se você está procurando por Clínica para Animais em Alto de Pinheiros está no lugar certo. A Dr. Patinhas após 6 anos de acompanhamento diário em cirurgias, enfermidades, distúrbios e disfunções apresentadas pelas diversas raças e espécies de animais, obteve a grande oportunidade de compartilhar seus conhecimentos de maneira totalmente independente, formando seu próprio centro de atendimento veterinário. </p>
<p>A Dr Patinhas além de ser uma das principais empresas do setor de Clinica Veterinária tem como foco trazer o que se tem de melhor nesse ramo. E por ser uma excelente empresa, se dispõe a prestar uma ótima assessoria, tanto para Clínica para Animais em Alto de Pinheiros, quanto para Clínica veterinária noturna, Banho para cachorro, Oftalmologia para animais, Raio X em cachorro e Oftalmo para cachorros e Gatos. Conte com a gente, pois temos uma equipe de sucesso esperando pelo seu contato.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>