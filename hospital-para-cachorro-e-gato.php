<?php
    $title       = "Hospital para Cachorro e gato";
    $description = "O hospital para cachorro e gato segue como a fonte de solução mais eficiente e completa para problemas corriqueiros ou críticos com animais. Venha saber mais.
";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    <?php // include "includes/modal-orcamento-qsmi.php"; ?>

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php // echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>Pois bem, o hospital para cachorro e gato nada mais é que o centro de atendimento completo para qualquer tipo de necessidade de seu pet, isso porque o espaço oferece não somente a possibilidade de consulta agendada para avaliações periódicas dos animais, como também atuações de maior complexidade e aprofundamento.</p>
<p>Sabendo dessa importância, a Dr. Patinhas se destaca como hospital para cachorro e gato, pois oferece os exames laboratoriais, centro cirúrgico e tudo mais que um bom hospital para animais deve oferecer. Além disso, oferecemos um atendimento totalmente personalizado a cada um, com todo cuidado e atenção necessários.</p>
<h2>Mais informações sobre hospital para cachorro e gato:</h2>
<p>Um bom hospital para cachorro e gato conta com a atuação de especialistas capaz de garantir a saúde e bem estar dos animais.</p>
<p>Em geral, um hospital para cachorro e gato  disponibiliza atendimento clínico geral e de especialidades como ortopedia, neurologia, dermatologia, cardiologia, oncologia e fisioterapia, entre outras. Além disso, deve possuir infraestrutura para realização de procedimentos cirúrgicos, diagnósticos por imagem e exames adequados para cachorros e gatos.</p>
<p>Em suma, um hospital para cachorro e gato é quase sempre a opção número um entre os proprietários de pets na hora de solucionar problemas de saúde de seus animaizinhos de estimação. </p>
<p>É comum, encontrar hospital para cachorro e gato espalhados por todo o País que oferecem tecnologia de ponta no diagnóstico e no tratamento de animais como cães e gatos, estes hospitais ficam abertos 24 horas por dia, garantindo socorro e alívio para pets de todos os tipos e raças.</p>
<p>Lembrando que, o hospital para cachorro e gato é o preferido entre os que buscam por cuidados para seus pets, já que, por serem abrigados em grandes centros são capazes de juntar em um só lugar os serviços de atendimento clínico, exames diagnósticos e até cirurgias.</p>
<h2>Dr. Patinhas - A solução ideal para o seu pet </h2>
<p>Com 6 anos de acompanhamento diário em cirurgias, enfermidades, distúrbios e disfunções apresentadas pelas diversas raças e espécies de animais, nós agregamos valores acessíveis e justos em conjunto com diversas formas de pagamento para facilitar a sua parceria.</p>
<p>Por fim, vale frisar que nós possuímos uma completa infraestrutura que permite atendimento rápido em casos de urgência, além da equipe de médicos veterinários amplamente treinada para este tipo de situação. Deixe os detalhes conosco e desfrute de um trabalho bem feito. Ligue agora mesmo, tire todas as suas dúvidas e realize um orçamento sem compromisso.</p>

                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>