<?php
    $title       = "Tosa em cães no Sacomã";
    $description = "O cuidado com os pêlos, através da tosa em cães no Sacomã, contribui para a limpeza e diminui o risco de doenças aos nossos bichinhos de estimação.
";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    <?php // include "includes/modal-orcamento-qsmi.php"; ?>

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php // echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>A tosa em cães no Sacomã é indicada para todos os pets. No entanto, se você tem um cachorro de pêlo longo que precise de tosa, a recomendação é que você faça uma tosa média, que mantenha o cachorro higiênico mas ao mesmo tempo não tire a função do pelo do cão.</p>
<p>Pois bem, sabendo disso, a tosa em cães no Sacomã da Dr. Patinhas se destaca no mercado, pois, possuímos uma vasta experiência neste ramo e contamos com uma equipe unida e organizada que busca superar as suas expectativas, apresentando soluções modernas.</p>
<h2>Por que a tosa em cães no Sacomã é importante?</h2>
<p>A tosa em cães no Sacomã higiênica, por exemplo, é feita para manter a higiene e a limpeza do cachorro, ela consiste em aparar os pelos das patas e aparar a área íntima do cachorro, pois essa região acaba ficando com resquícios, muitas vezes de urina e fezes, concentrando um cheiro ruim e sujeira.</p>
<p>A freqüência para tosa em cães no Sacomã vai variar de raça para raça, podendo ser no período de 45 dias a 3 meses. </p>
<p>É importante frisar que, a tosa em cães no Sacomã e banhos freqüentes no seu cachorro não são recomendados para não tirar a proteção natural da pele dele, isso pode gerar dermatites e inclusive aumentar o cheiro do cachorro.</p>
<p>O ideal é procurar um lugar por indicação de algum conhecido, pois as chances de você acertar são maiores também é importante que você não fique trocando de tosador, pois o cachorro acaba se acostumando com o profissional e a tosa em cães no Sacomã fica menos estressante pra ele.</p>
<h2>Tosa em cães no Sacomã? Chame Dr. Patinhas!</h2>
<p>A nossa clínica conta com uma equipe treinada e preparada para cuidar da estética do seu pet com todo amor. Além disso, agregamos valores acessíveis e justos em conjunto com diversas formas de pagamento para facilitar a sua contratação. E ainda, a qualquer hora do dia, estamos disponíveis para tirar todas as suas dúvidas.</p>
<p>Desde o início, nós estabelecemos uma relação de transparência e comprometimento com os nossos clientes para que ambas as partes se sintam confortáveis e seguras neste momento. Por fim, nós possuímos uma completa infraestrutura que permite atendimento rápido em casos de urgência, além da equipe de médicos veterinários amplamente treinada para este tipo de situação. Deixe os detalhes conosco e desfrute de um trabalho bem feito. Ligue agora mesmo e saiba mais.</p>

                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>