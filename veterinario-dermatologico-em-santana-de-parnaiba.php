<?php
$title       = "Veterinário Dermatológico em Santana de Parnaíba";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Pois bem, os deveres do Veterinário Dermatológico em Santana de Parnaíba abrangem a avaliação geral do pet, antes mesmo de começar o tratamento, onde o profissional fará uma anamnese, entrevistando o tutor para que o mesmo informe tudo que ocorre com o animal. Problemas dermatológicos podem ter variadas causas, por isso demandam uma investigação completa sobre os hábitos de vida do animal. </p>
<p>Como uma empresa especializada em Clinica Veterinária proporcionamos sempre o melhor quando falamos de Oftalmo para cachorros e Gatos, Produtos Hydra para seu animal, Acupuntura para animais, Cirurgia de tártaro em cães e Vacina contra Raiva. Com potencial necessário para garantir qualidade e excelência em Veterinário Dermatológico em Santana de Parnaíba com custo x benefício justos no mercado sem diminuir a qualidade de nossa especialidade. Nós da empresa Dr Patinhas trabalhamos com os melhores valores do mercado em que atuamos.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>