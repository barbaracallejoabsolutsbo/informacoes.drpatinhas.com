<?php
$title       = "Veterinário Dermatológico no Tatuapé";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Fornecemos Veterinário Dermatológico no Tatuapé, com um atendimento personalizado. Além disso, a nossa equipe trabalha de forma unida e organizada para fornecer o que há de melhor aos clientes, de acordo com as evoluções do mercado.  O veterinário nada mais é que o médico responsável pela saúde de pele dos animais de estimação, no qual nesta área estão incluídos também os cuidados com as unhas e as orelhas.</p>
<p>Com sua credibilidade no mercado de Clinica Veterinária, proporcionando com qualidade, viabilidade e custo x benefício seja em Veterinário Dermatológico no Tatuapé quanto em Veterinário Dermatológico, Cirurgia em Animais, Internação para Cachorro, Oftalmo para cachorros e Gatos e Hospital para Cachorro e gato, podendo dessa forma garantir seu sucesso no segmento em que atua de forma idônea e com altíssimo nível de qualidade a Dr Patinhas é a opção número um para você garantir o melhor no que busca!</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>