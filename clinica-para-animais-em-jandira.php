<?php
$title       = "Clínica para Animais em Jandira";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Pois bem, uma boa Clínica para Animais em Jandira deve treinar médicos veterinários capacitados a realizar exames clínicos criteriosos, colheita de materiais biológicos, cirurgia geral e específica, interpretação de exames laboratoriais, permitindo que seja oferecido serviço de qualidade por parte do profissional. Para melhor atender as necessidades de nossos pacientes, contamos com excelentes colaboradores </p>
<p>Além de sermos uma empresa especializada em Clínica para Animais em Jandira disponibilizamos uma equipe de profissionais altamente competente a fim de prestar um ótimo atendimento em Oncologia em cães e Gatos, Raio X em cachorro, Produtos Hydra para seu animal, Ultrassom em cachorro e Banho para gato. Com a ampla experiência que a equipe Dr Patinhas possui na atualidade, garantimos um constante desenvolvimento voltado a melhorar ainda mais nos destacados entre as principais empresas do mercado de Clinica Veterinária.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>