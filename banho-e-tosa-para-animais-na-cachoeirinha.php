<?php
$title       = "Banho e tosa para animais na Cachoeirinha";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>O Banho e tosa para animais na Cachoeirinha é realizado por um profissional que cuida da beleza e do bem estar completo do pet, pois, ele que dá banho, corta e apara os pelos do animal e cuida dos processos de higienização. O Dr. Patinhas não hesita em fazer um bom trabalho e conta com profissionais especializados para fornecer o atendimento personalizado ao cliente. </p>
<p>Como uma especialista em Clinica Veterinária, a Dr Patinhas se destaca dentre as demais empresas quando o assunto é Banho e tosa para animais na Cachoeirinha, uma vez que, nossa empresa conta com mão de obra com amplo conhecimento em diferentes ramificações do segmento, assim como em Ecocardiograma em Cachorro, Clínica de Pet Shop, Melhor pet shop para banho e tosa, Vacinas para animais e Castração de Gato. Temos os mais competentes profissionais e as principais ferramentas do mercado de modo a prestar o melhor atendimento possível.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>