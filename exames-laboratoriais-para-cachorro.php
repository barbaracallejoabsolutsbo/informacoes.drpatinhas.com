<?php
    $title       = "Exames laboratoriais para Cachorro";
    $description = "Com os nossos exames laboratoriais para cachorro será possível identificar potenciais problemas que podem se desenvolver, além de tratar eventuais situações.
";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    <?php // include "includes/modal-orcamento-qsmi.php"; ?>

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php // echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>Os exames laboratoriais para cachorro são extremamente necessários para a prevenção de futuras doenças, garantindo assim, maior qualidade de vida de seu pet. Sabendo disso, é importante que os profissionais sejam qualificados e os exames sejam feitos com qualidade e competência.</p>

<p>Pois bem, a Dr. Patinhas se destaca nesse ramo, pois, os nossos exames laboratoriais para cachorro são excelentes colaboradores que ao serem requisitados, prontamente se deslocam à nossa clínica para efetuarem os exames necessários.</p>

<h2>Saiba mais sobre a importância dos exames laboratoriais para cachorro:</h2>

<p>Com exames laboratoriais para cachorro de rotina podemos identificar uma série de problemas ainda no início. E assim, fazer um tratamento adequado.</p>
<p>Em geral, os exames laboratoriais para cachorro  são mais do que idas ao veterinário, pois, é a partir dos exames que alguns diagnósticos podem ser feitos e, acima de tudo, estudados mais a fundo.</p>
<p>Um cachorro, de maneira geral, não demonstra alguns tipos de dores ou desconfortos. E quando ele demonstra, infelizmente, já pode ser tarde demais.</p>
<p>Por isso, os exames laboratoriais para cachorro de rotina vão ajudar a identificar possíveis problemas que ainda vão se desenvolver ou que estão em fase bem inicial.</p>
<p>É importante salientar que, os cachorros idosos mais sensíveis a alguns problemas e doenças. Por mais que sua vida seja saudável, eles podem ser mais suscetíveis a doenças variadas. Por isso, levá-los para uma consulta de rotina é fundamental. Pois, caso haja a necessidade de realizar exames laboratoriais para cachorro, as prevenções podem ser colocadas em prática e o animal terá uma melhor qualidade de vida.</p>
<h2>Por que devo fazer os exames laboratoriais para cachorro com a Dr. Patinhas?</h2>
<p>Há mais de 6 anos atuando na área, a nossa equipe trabalha de forma unida e organizada para não somente atender a necessidade do cliente, mas também, superar as suas expectativas apresentando soluções completas ao seu pet, seja de maneira estética e cirúrgica.</p>
<p>Vale lembrar que, pensando no bem estar completo de todos os envolvidos nestes serviços, nós visamos agregar valores acessíveis e justos em conjunto com diversas formas de pagamento para facilitar a sua parceria.</p>
<p>Por fim, destacamos que nós possuímos uma completa infraestrutura que permite atendimento rápido em casos de urgência, além da equipe de médicos veterinários amplamente treinada para este tipo de situação. No momento em que entrar em contato conosco, você notará que encontrou a clínica ideal para se tornar parceiro. Ligue agora mesmo e tire todas as suas dúvidas.</p>

                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>