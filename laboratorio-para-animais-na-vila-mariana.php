<?php
    $title       = "Laboratório para animais na Vila Mariana";
    $description = "A Dr. Patinhas realiza exame de laboratório para animais na Vila Mariana e presta auxílio ao clínico e cirurgião de diversos animais, inclusive exóticos.
";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    <?php // include "includes/modal-orcamento-qsmi.php"; ?>

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php // echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>O laboratório para animais na Vila Mariana da Dr. Patinhas se destaca no mercado por apresentar soluções completas aos pets, de forma rápida e eficiente, sem estressar o cliente e o seu animal de estimação. Com anos de experiência neste ramo, contamos com profissionais excepcionais que estão sempre atentos às atualizações do mercado para fornecer o que há de melhor e mais moderno.</p>

<p>Em suma, para melhor atender as necessidades dos nossos pacientes, contamos com excelentes colaboradores que ao serem requisitados, prontamente se deslocam à nossa clínica para efetuarem o exame de laboratório para animais na Vila Mariana necessário.</p>

<h2>Importância do laboratório para animais na Vila Mariana:</h2>

<p>O exame do laboratório para animais na Vila Mariana é importante, pois, oferece ferramentas para o diagnóstico de diversas doenças que acometem cães e gatos. </p>

<p>O laboratório para animais na Vila Mariana, em geral, permite melhor direcionamento para suspeitas clínicas e exclusão de diversos diagnósticos diferenciais, tendo em vista que o resultado neste estabelecimento é confiável e de qualidade depende de vários fatores.</p>

<p>Em geral, o exame de laboratório para animais na Vila Mariana tem como principal objetivo auxiliar o clínico veterinário a diagnosticar doenças, levando informações e recursos que possam ser aplicados na prática terapêutica da medicina veterinária e a desenvolver um perfil de saúde dos cães e gatos.</p>

<p>Dentre os exames de laboratórios mais comuns, podemos citar:</p>
<ul>
<li>
<p>Hemograma;</p>
</li>
<li>
<p>Urinálise;</p>
</li>
<li>
<p>Microbiologia;</p>
</li>
<li>
<p>Parasitologia;</p>
</li>
<li>
<p>Bioquímica, entre outros.</p>
</li>
</ul>
<p>Lembrando que, qualquer alteração pode comprometer o resultado do exame de laboratório para animais na Vila Mariana. </p>

<h2>Bem vindo a Dr. Patinhas com laboratório para animais na Vila Mariana de qualidade!</h2>
<h2></h2>
<p>Dentre os diversos serviços que oferecemos, podemos citar: vacinas importadas, medicamentos, castração de animais, limpeza de tártaro, piometra e cesárea, além do excelente e cuidadoso tratamento no banho e tosa. Ressaltamos ainda que, nós possuímos uma completa infra-estrutura que permite atendimento rápido em casos de urgência, além da equipe de médicos veterinários amplamente treinada para este tipo de situação.</p>
<p>Além disso, nós visamos agregar valores acessíveis e justos em conjunto com diversas formas de pagamento para facilitar a sua parceria. No momento em que entrar em contato conosco, você notará que fez a escolha certa. A qualquer hora do dia, nós estamos disponíveis para tirar todas as suas dúvidas e fornecer todo o suporte necessário, com presteza e atenção. Ligue agora mesmo e realize um orçamento totalmente sem compromisso com a nossa equipe.</p>

                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>