<?php
$title       = "Oncologia em cães e Gatos na Vila Sonia";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Existe ainda o chamado tratamento paliativo, indicado para casos crônicos da Oncologia em cães e Gatos na Vila Sonia, em que o tumor está avançado. Neste sentido, não há possibilidade de cura e os cuidados são apenas para aliviar os sofrimentos. Por fim, é imprescindível que o profissional conheça as origens do problema e as formas de diagnóstico para que a doença seja diagnosticada com precisão.</p>
<p>Se você está em busca de Oncologia em cães e Gatos na Vila Sonia tem preferência por uma empresa com conhecimento e custo-benefício, a Dr Patinhas é a melhor opção para você. Com uma equipe formada por profissionais amplamente qualificados e dedicados para oferecer soluções personalizadas para cada cliente que busca pela excelência. Entre em contato com a gente e saiba mais sobre Clinica Veterinária e todos os nossos serviços como Castração de Gato, Melhor pet shop para banho e tosa, Internação para Cachorro, Clínica veterinária de Domingo e Exames para Cachorro.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>