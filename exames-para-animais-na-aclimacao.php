<?php
    $title       = "Exames para animais na Aclimação";
    $description = "Consulte um profissional, pois, os resultados de exames para animais na Aclimação vai depender de fatores como a idade e o histórico médico do pet.
";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    <?php // include "includes/modal-orcamento-qsmi.php"; ?>

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php // echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>Na Dr. Patinhas, os exames para animais na Aclimação são completos e eficientes. Há mais de 6 anos atuando neste ramo, contamos com profissionais excepcionais que trabalham de forma unida e organizada para não somente atender a necessidade do cliente e seu pet, mas também, superar todas as suas expectativas apresentando soluções completas e rápidas.</p>

<p>Vale ressaltar que, os exames para animais na Aclimação são importantes para o veterinário entender e saber mais sobre como estão as necessidades básicas do seu pet, de modo a entender isso como uma pista ou não para um problema mais sério.</p>
<h2>Importância dos exames para animais na Aclimação:</h2>
<p>Os exames para animais na Aclimação vem sendo cada vez mais procurados por aqueles que se preocupam com seus pets ou animais de produção. Atualmente, é possível perceber que o mercado de análises clínicas é bastante promissor já que, quando o assunto é saúde dos animais.</p>
<p>É importante frisar que, realizar os exames para animais na Aclimação é fundamental  para a identificação prévia de certas enfermidades, garantindo que a saúde do seu pet esteja sempre em dia. </p>
<p>Confira abaixo os mais comuns exames para animais na Aclimação:</p>
<ul>
<li>
<p>Hemograma – Serve para indicar processos inflamatórios e infecciosos, além de evidenciar alterações com sugestão da medula óssea. É recomendado neste, jejum de 8 a 12 horas;</p>
</li>
<li>
<p>Exame de urina – Auxilia no diagnóstico de diabetes e outras doenças endócrinas, fornecendo informações necessárias sobre o funcionamento do rim do animal;</p>
</li>
<li>
<p>Função hepática – Esses exames para animais na Aclimação identifica possíveis alterações e doenças no fígado e está entre os mais importantes a serem realizados, entre outros diversos.</p>
</li>
</ul>
<p>Lembrando que, quando se tem conhecimento de doenças prévias e dos resultados de exames antigos, é possível solicitar testes mais específicos e direcionar uma investigação mais precisa para o tratamento adequado, se necessário, do seu pet.</p>
<h2>Dr. Patinhas e os exames para animais na Aclimação:</h2>
<p>Primeiramente, é importante salientar que dentre os nossos serviços, oferecemos vacinas importadas, medicamentos, castração de animais, limpeza de tártaro, piometra e cesárea, além do excelente e cuidadoso tratamento no banho e tosa. Além disso, visamos agregar valores acessíveis e justos em conjunto com diversas formas de pagamento para facilitar a sua parceria. </p>
<p>Diante de todos esses fatores, ressaltamos que, a qualquer hora do dia, a nossa equipe está disponível para tirar todas as suas dúvidas e fornecer todo o suporte necessário, com presteza e atenção. Venha conferir.</p>

                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>