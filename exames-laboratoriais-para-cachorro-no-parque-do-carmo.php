<?php
$title       = "Exames laboratoriais para Cachorro no Parque do Carmo";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Os Exames laboratoriais para Cachorro no Parque do Carmo  são mais do que idas ao veterinário, pois, é a partir dos exames que alguns diagnósticos podem ser feitos e, acima de tudo, estudados mais a fundo. Um cachorro, de maneira geral, não demonstra alguns tipos de dores ou desconfortos. E quando ele demonstra, infelizmente, já pode ser tarde. Por isso, os exames vão ajudar a identificar possíveis problemas.</p>
<p>Com uma ampla atuação no segmento, a Dr Patinhas oferece o melhor quando falamos de Exames laboratoriais para Cachorro no Parque do Carmo proporcionando aos seus clientes a máxima qualidade e desempenho em Laboratório para animais, Clínica de Estética animal, Clínica veterinária noturna, Melhor pet shop para banho e tosa e Oncologia em cães e Gatos, uma vez que, a nossa equipe de profissionais que atuam para proporcionar aos clientes sempre o melhor. Somos a empresa que mais se destaca quando se trata de Clinica Veterinária.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>