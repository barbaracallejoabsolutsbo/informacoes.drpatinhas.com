<?php
$title       = "Tosa em cães em CECAP - Guarulhos";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>A Tosa em cães em CECAP - Guarulhos é indicada para todos os pets. No entanto, se você tem um cachorro de pêlo longo que precise de tosa, a recomendação é que você faça uma tosa média, que mantenha o cachorro higiênico mas ao mesmo tempo não tire a função do pelo do cão. A Dr. Patinhas se destaca no mercado, pois, possuímos uma vasta experiência neste ramo e contamos com uma equipe unida e organizada.</p>
<p>Sendo uma das empresas mais confiáveis no ramo de Clinica Veterinária, a Dr Patinhas ganha destaque por ser confiável e idônea quando falamos não só de Tosa em cães em CECAP - Guarulhos, mas também quando o assim é Laboratório para animais, Exames laboratoriais para Cachorro, Oftalmo para cachorros e Gatos, Vacina contra Raiva e Exames para animais. Pois, aqui tudo é realizado por um time de profissionais experientes e que trabalham para oferecer a todos os clientes as melhores soluções para você.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>