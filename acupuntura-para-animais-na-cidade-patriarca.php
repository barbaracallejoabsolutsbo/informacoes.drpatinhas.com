<?php
$title       = "Acupuntura para animais na Cidade Patriarca";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Está procurando por Acupuntura para animais na Cidade Patriarca? Pois, você está no lugar certo. A equipe Dr Patinhas é especializada há mais de 6 anos neste ramo, garantindo acompanhamento diário em todos os procedimentos fornecidos a diversas raças e espécies de animais. A qualquer hora do dia, nós estamos disponíveis para tirar todas as suas dúvidas e fornecer todo o suporte necessário, com presteza e atenção.</p>
<p>Você procura por Acupuntura para animais na Cidade Patriarca? Contar com empresas especializadas no segmento de Clinica Veterinária é sempre a melhor saída, já que assim temos a segurança de excelência e profissionalismo. Pensando assim, a empresa Dr Patinhas é a opção certa para quem busca a soma de qualidade, comprometimento e agilidade em Oftalmo para cachorros e Gatos, Exames para Gatos, Melhor pet shop para banho e tosa, Banho para cães e Gatos e Laboratório para animais e ainda, um atendimento personalizado.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>