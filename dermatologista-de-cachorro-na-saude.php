<?php
    $title       = "Dermatologista de Cachorro na Saúde";
    $description = "Dependendo do diagnóstico, o dermatologista de cachorro na Saúde irá indicar o melhor e mais eficaz tratamento, de forma rápida e completa. Venha saber mais.
";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    <?php // include "includes/modal-orcamento-qsmi.php"; ?>

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php // echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>Após 6 anos de acompanhamento diário em cirurgias, enfermidades, distúrbios e disfunções apresentadas pelas diversas raças e espécies de animais, nos destacamos em todos os serviços que oferecemos, bem como contamos com profissionais qualificados, incluindo o dermatologista de cachorro na Saúde.</p>

<p>Pois bem, o dermatologista de cachorro na Saúde nada mais é que o médico responsável pela saúde da pele dos pets. Isso inclui também os cuidados com outras áreas, como unhas e orelhas. Mas não se preocupe, pois, a qualquer hora do dia, nós estamos disponíveis para tirar todas as suas dúvidas.</p>

<h2>Informações detalhadas sobre dermatologista de cachorro na Saúde:</h2>

<p>Os deveres habituais de um dermatologista de cachorro na Saúde incluem avaliar o animal antes do tratamento, realizando uma inspeção completa. Durante a consulta, também é feita uma entrevista com o tutor, para maiores informações coletadas durante o exame clínico.</p>
<p>Além disso, cabe ao dermatologista de cachorro na Saúde, a coleta de amostras para exames e a realização de cirurgias dermatológicas. Lembrando que, a interpretação correta dos resultados dos exames também é responsabilidade do veterinário dermatologista de cachorro na Saúde. Por isso, um bom treinamento é imprescindível para que os diagnósticos sejam feitos com precisão e rapidez.</p>
<p>Alergias, piodermites, infecções e feridas, descamação, e seborreia, caspas, vermelhidão da pele e dos ouvidos, tumores e micoses são alguns exemplos de doenças que requerem a avaliação de um dermatologista de cachorro na Saúde.</p>
<p>Na maioria dos casos, eles são os principais responsáveis pelas doenças de pele. A sarna, por exemplo, é causada por um ácaro que entra nas camadas profundas da pele, causando coceira. Nos cães, as lesões são mais visíveis na cabeça e nas orelhas. Consulte um profissional.</p>
<h2>Conte com o dermatologista de cachorro na Saúde da Dr. Patinhas!</h2>
<p>A Dr. Patinhas sempre se dedicou aos animais como um ser vivo e nunca como um produto. Entendemos que é possível prestar um atendimento de ótima qualidade sem a necessidade de se cobrar valores muitas vezes inacessíveis a muitos clientes. Por isso, nós agregamos valores acessíveis e justos em conjunto com diversas formas de pagamento para facilitar a sua parceria.</p>
<p>É importante frisar que, desde o primeiro contato, nós estabelecemos uma relação de transparência e comprometimento para que ambas as partes se sintam confortáveis e seguras. Deixe os detalhes conosco e desfrute de um trabalho bem feito. Não perca mais tempo e nem a oportunidade de se tornar o nosso parceiro. Ligue agora mesmo e faça um orçamento sem compromisso. Esperamos por você.</p>

                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>