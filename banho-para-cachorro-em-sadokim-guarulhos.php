<?php
$title       = "Banho para cachorro em Sadokim - Guarulhos";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>O Banho para cachorro em Sadokim - Guarulhos em filhotes: O banho em pet shop só é válido após seu cão ter sido vacinado e vermifugado, desse modo o primeiro banho dele será em casa. Alguns veterinários podem indicar banho, mesmo em casa, apenas após o término das vacinas e vermifugação. Primeiramente, é válido ressaltar que, nós possuímos uma completa infra-estrutura que permite atendimento rápido em casos de urgência.</p>
<p>Por ser a principal empresa quando falamos de Clinica Veterinária, a Dr Patinhas se dispõe a adquirir os melhores e mais modernos recursos para atender seus clientes sempre da melhor forma. Possuindo como objetivo viabilizar tanto Oncologia em cães e Gatos, Exames laboratoriais para Cachorro, Raio X em gatos, Internação para Cachorro e Médico Veterinário, quanto como Banho para cachorro em Sadokim - Guarulhos mantendo a qualidade e a eficiência que você deseja. Entre em contato e faça uma cotação.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>