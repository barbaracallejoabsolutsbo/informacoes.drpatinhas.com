<?php
    $title       = "Hospital para cães e gatos";
    $description = "O hospital para cães e gatos oferece diversas especialidades de médicos veterinários, garantindo o tratamento ideal ao cliente, de forma eficiente e rápida.
";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    <?php // include "includes/modal-orcamento-qsmi.php"; ?>

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php // echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>O melhor hospital para cães e gatos chama-se Dr. Patinhas. Com anos de experiência neste ramo, não hesitamos em fazer um bom trabalho e estamos sempre disponíveis para tirar as dúvidas dos nossos clientes, com toda presteza e atenção necessária.</p>

<p>É importante frisar que, nos destacamos como hospital para cães e gatos, pois, todos os nossos profissionais passam por um treinamento adequado para fornecer o atendimento personalizado que o cliente e seu pet procura e merece. Deixe os detalhes conosco e desfrute de um trabalho bem feito.</p>

<h2>Maiores informações sobre hospital para cães e gatos:</h2>

<p>Pois bem, um hospital para cães e gatos nada mais é que uma instituição pública ou mista que cobra por alguns serviços ou exigem cobertura médica particular.</p>
<p>Geralmente, o hospital para cães e gatos possui o horário de funcionamento de 24 horas, para internação e atendimento de emergência. Lembrando que, a principal diferença entre um hospital para cães e gatos e uma clínica veterinária está no alcance da atenção. </p>
<p>Dentre as diversas obrigatoriedades dele, podemos citar: </p>
<p>• Atendimento;<br />• Área de diagnóstico (laboratório de análises clínicas, radiologia e ultrassonografia, entre outros);<br />• Departamento cirúrgico – com estrutura de urgência;<br />• Internação – com acomodações de isolamento e estrutura de higienização;<br />• setor de sustentação, entre outros requisitos.</p>
<p>É importante ressaltar que o hospital para cães e gatos foca no atendimento especializado ao animal, de forma a resolver o problema ou proporcionar um tratamento a longo prazo. Por isso, procure um profissional adequado antes de tomar quaisquer decisões.</p>
<h2>O melhor hospital para cães e gatos é Dr. Patinhas!</h2>
<p>Com valores acessíveis e justos em conjunto com diversas formas de pagamento, ressaltamos que o nosso consultório é confortável, climatizado e higienizado, para o seu pet. Além disso, possuímos uma infraestrutura que permite atendimento rápido em casos de urgência, além da equipe de médicos veterinários amplamente treinada para este tipo de situação.</p>
<p>Vale frisar que, desde o primeiro contato, é estabelecida uma relação de transparência e comprometimento para que ambas as partes se sintam seguras e mais confortáveis nesta relação. Não perca mais tempo e nem a oportunidade de se tornar parceiro de uma empresa que prioriza e respeita você. Entre em contato agora mesmo e realize um orçamento sem compromisso. Esperamos por você para garantir todo o cuidado com o seu pet, desde o tratamento estético até especialidades em cirurgias delicadas. Venha conferir.</p>

                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>