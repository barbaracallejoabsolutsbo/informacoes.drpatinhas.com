<?php
$title       = "Ecocardiograma em Cachorro no Tucuruvi";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>O Ecocardiograma em Cachorro no Tucuruvi é um tratamento que permite ondas sonoras ao coração do bichinho, captando os ecos formados e os transforma em imagens que são registradas e visualizadas em um monitor que possibilita a leitura e o diagnóstico por parte do médico veterinário. Lembrando que esse exame não é um procedimento invasivo, não sendo necessário realizar nenhum tipo de corte.</p>
<p>Buscando por uma empresa de credibilidade no segmento de Clinica Veterinária, para que, você que busca por Ecocardiograma em Cachorro no Tucuruvi, tenha a garantia de qualidade e idoneidade, contar com a Dr Patinhas é a opção certa. Aqui tudo é feito por um time de profissionais com amplo conhecimento em Vacina contra Raiva, Banho para cachorro, Laboratório para animais, Exames laboratoriais para Cachorro e Ecocardiograma em Cachorro para assim, oferecer a todos os clientes as melhores soluções.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>