<?php
$title       = "Produtos Hydra para seu animal na Luz";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Uma das mais reconhecidas marcas de mercado pet, a Pet Society oferece Produtos Hydra para seu animal na Luz aliando o amplo conhecimento científico nas áreas de ingredientes e formulações seguras às necessidades do mercado veterinário.  Em geral, ela cria produtos hydra exclusivos de alta qualidade e traz novos conceitos que beneficiem e ajudem a modernizar o mercado pet. </p>
<p>Trabalhando há anos no mercado voltado à Clinica Veterinária, a Dr Patinhas é uma empresa de grande experiente e qualificada quando se trata de Exames para Cachorro, Diagnóstico laboratório Veterinário, Produtos Hydra para seu animal, Melhor pet shop para banho e tosa e Clínica veterinária noturna e Produtos Hydra para seu animal na Luz. Em constante busca da satisfação de nossos clientes, atuamos no mercado com o intuito de fornecer para todos que buscam pela nossa qualidade o que há de mais moderno e eficiente no segmento.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>