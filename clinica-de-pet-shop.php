<?php
    $title       = "Clínica de Pet Shop";
    $description = "Somos uma clínica de pet shop especializada que conta com uma equipe treinada e preparada para cuidar da estética do seu pet com todo amor e atenção.";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    <?php // include "includes/modal-orcamento-qsmi.php"; ?>

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php // echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>Bem vindo a melhor clínica de pet shop. Há mais de 6 anos atuando no ramo, a Dr. Patinhas garante o acompanhamento diário em cirurgias, enfermidades, distúrbios e disfunções apresentadas pelas diversas raças e espécies de animais. Além disso, todos os nossos profissionais oferecem o atendimento personalizado que o cliente procura e merece para seu pet.</p>

<p>Em geral, a nossa clínica de pet shop conta com uma equipe treinada e preparada para cuidar da estética do seu pet com todo amor, bem como outros procedimentos mais delicados, com toda estrutura necessária e moderna disponível no mercado.</p>

<h2>Mais detalhes sobre clínica de pet shop:</h2>

<p>Uma clínica de pet shop pode oferecer inúmeras opções, tanto em cuidados veterinários quanto em alimentação, comércio de produtos estéticos, roupas, educação, transporte, hospedagem e até mesmo serviços funerários.<br /><br />Em busca de produtos e serviços mais específicos, os donos de animais geralmente recorrem a uma clínica de pet shop, pois neles pode-se encontrar praticamente tudo.</p>
<p>Desta forma, economiza-se tempo e dinheiro, sem mencionar que uma clínica de pet shop busca e entrega os pets para vacinas, banhos e tosas, além de disponibilizarem o serviço de entrega de produtos e mercadorias.</p>
<p>Lembrando que, em uma clínica de pet shop pode-se encontrar rações, produtos veterinários, filhotes de animais, além de serviços como banho e tosa, atendimento clínico, hospedagem, transporte, dentre outros. Por fim, um pet shop deve funcionar em horário comercial e, principalmente, aos sábados, quando os proprietários têm tempo de levar os seus animais para um banho e tosa, ou para qualquer atendimento.</p>
<h2>Dr. Patinhas – A melhor clínica de pet shop</h2>
<p>No momento em que entrar em contato conosco, você notará que fez a escolha certa. Pensando em seu bem estar e do seu pet completo, nós agregamos valores acessíveis e justos em conjunto com diversas formas de pagamento em todos os nossos serviços.</p>
<p>Ao passar por uma consulta seu pet já pode realizar os exames laboratoriais aqui na Dr. Patinha, com muito mais praticidade e conforto. Além disso, nós oferecemos praticidade e garantia de qualidade com nossa farmácia pet.</p>
<p>Por fim, destacamos que desde o primeiro contato, nós estabelecemos uma relação de transparência e comprometimento para que ambas as partes se sintam seguras e confortáveis nesta parceria. Deixe os detalhes com a nossa equipe e desfrute de um trabalho bem feito. Venha conferir, ligue agora mesmo e saiba mais. Estamos esperando por você agora mesmo.</p>

                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>