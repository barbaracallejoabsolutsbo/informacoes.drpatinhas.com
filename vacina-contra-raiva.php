<?php
    $title       = "Vacina contra Raiva";
    $description = "A raiva é uma zoonose que pode também afetar o ser humano e por isso os cuidados se redobram no combate e prevenção da doença, com a vacina contra raiva.
";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    <?php // include "includes/modal-orcamento-qsmi.php"; ?>

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php // echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>Na Dr. Patinhas, oferecemos diversos serviços, entre eles podemos citar vacina contra raiva importada, medicamentos, castração de animais, limpeza de tártaro, piometra e cesárea, além do excelente e cuidadoso tratamento no banho e tosa. Além disso, todos os nossos profissionais passam por um treinamento adequado para fornecer o atendimento personalizado que o cliente procura e merece.</p>
<p>A vacina contra raiva deve ser dada aos cachorros aos 6 meses de idade, ou de acordo com a recomendação do médico veterinário responsável. Lembrando que, geralmente é ministrada uma semana após a primeira dose da e o reforço deve ocorrer anualmente.</p>
<h2>Mais sobre vacina contra raiva:</h2>
<p>A raiva trata-se de uma infecção viral aguda que pode acometer animais e seres humanos, que contraem a doença por uma mordida causada por animais infectados, assim, a doença ataca o sistema nervoso central do hospedeiro, causando encefalite que evolui de forma bem rápida. </p>
<p>A raiva é uma doença facilmente contraída, principalmente, por animais que vivem em locais com diversos exemplares silvestres. Por isso, a vacina contra raiva é importante, tendo em vista que esta doença deixa os animais vulneráveis também correndo um grande risco.</p>
<p>A vacina contra raiva fornece total proteção contra essa doença potencialmente fatal. Além disso, ela é obrigatória para cães e gatos. </p>
<p>Apesar de ser uma doença controlada, a raiva é muito grave e causa grande preocupação aos donos de animais de estimação. Por isso, a vacina contra raiva é ainda a única forma de prevenir a enfermidade e manter os pets saudáveis. Consulte um profissional.</p>
<h2>Vacina contra raiva na Dr. Patinhas!</h2>
<p>Nós oferecemos valores acessíveis e justos em conjunto com diversas formas de pagamento para facilitar a sua parceria e contratação. Após 6 anos de acompanhamento diário em cirurgias, enfermidades, distúrbios e disfunções apresentadas pelas diversas raças e espécies de animais, conquistamos o nosso espaço no ramo e estamos sempre atentos às atualizações do mercado para fornecer o que há de melhor.</p>
<p>Por fim, ressaltamos que atendemos animais exóticos e estabelecemos, desde o início, uma relação de transparência e comprometimento para que ambas as partes se sintam seguras e confortáveis. Entendemos que é possível prestar um atendimento de ótima qualidade sem a necessidade de se cobrar valores muitas vezes inacessíveis a muitos clientes. Deixe os detalhes conosco, ligue agora mesmo e faça um orçamento.</p>

                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>