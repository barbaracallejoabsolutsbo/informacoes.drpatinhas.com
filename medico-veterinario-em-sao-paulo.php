<?php
    $title       = "Médico Veterinário em São Paulo";
    $description = "O médico veterinário em São Paulo também estuda aplicação de medidas de saúde pública no que se refere às doenças de animais transmissíveis ao homem.
";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    <?php // include "includes/modal-orcamento-qsmi.php"; ?>

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php // echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>Pensou em médico veterinário em São Paulo, pensou na Dr. Patinhas. Com uma vasta experiência neste ramo, não hesitamos em fazer um bom trabalho e buscamos superar as expectativas dos nossos clientes apresentando soluções rápidas, eficientes e modernas.</p>
<p>De forma sucinta, o médico Veterinário em São Paulo é responsável por tratar as doenças dos animais de pequeno e grande porte e ainda, os animais silvestres. Além disso, este profissional pode indicar métodos de prevenção e cuidados com os animais, responsável pelo bem estar dos mesmos.</p>
<h2>Áreas de atuação de um médico Veterinário em São Paulo:</h2>
<p>Primeiramente, é importante destacar que um médico Veterinário em São Paulo  só pode atuar com diplomas expedidos por escolas reconhecidas pelo Ministério da Educação (MEC) e aos profissionais estrangeiros que tenham revalidado seu diploma no Brasil, devidamente inscritos no Conselho Regional de Medicina Veterinária do estado que pretenda atuar.</p>
<p>Há diversos ramos em que um médico Veterinário em São Paulo pode atuar. Vamos conhecer alguns:</p>
<ul>
<li>
<p>Clínica médica: Quando o médico Veterinário em São Paulo opta por trabalhar nesse segmento, ele atuará no atendimento de pequenos ou grandes animais, visando à manutenção e à promoção da sua saúde;</p>
</li>
<li>
<p>Saúde pública: Aqui, o profissional deve garantir a prevenção e o controle de zoonoses, doenças que acometem os animais e podem contaminar os humanos, entre outros.</p>
</li>
</ul>
<p>Lembrando que, o médico Veterinário em São Paulo  deve exercer a responsabilidade técnica junto a estabelecimentos cujas atividades e prestação de serviços sejam privativas à Medicina Veterinária.</p>
<h2>Dr. Patinhas - Médico Veterinário em São Paulo confiável</h2>
<p>Para melhor atender as necessidades de nossos pacientes, contamos com excelentes colaboradores que ao serem requisitados, prontamente se deslocam à nossa clínica para efetuarem os exames necessários. Após 6 anos de acompanhamento diário em cirurgias, enfermidades, distúrbios e disfunções apresentadas pelas diversas raças e espécies de animais, nos tornamos referência e  prezamos pelo bem estar de nossos clientes e seus pets.</p>
<p>Tendo esses fatores em vista, nós visamos agregar valores acessíveis e justos em conjunto com diversas formas de pagamento para facilitar a sua parceria. Além disso, desde o início, é estabelecida uma relação de transparência e comprometimento para que ambas as partes se sintam confiáveis e seguras neste momento. Mas não se preocupe caso tenha dúvidas, pois, a qualquer hora do dia, nós estamos disponíveis para fornecer todo o suporte necessário, com presteza e atenção. Ligue agora mesmo e faça um orçamento sem compromisso.</p>

                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>