<?php
$title       = "Ultrassom em gatos na Vila Leopoldina";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>O Ultrassom em gatos na Vila Leopoldina  possui um aparelho que é a parte que encosta no corpo e emite ondas sonoras. As ondas se propagam pelo organismo até atingir tecidos de diferentes densidades, como fluidos, tecidos moles, gorduras e ossos. Desde o primeiro contato, nós estabelecemos uma relação de transparência e comprometimento para que ambas as partes se sintam seguras e mais confortáveis.</p>
<p>Além de sermos uma empresa especializada em Ultrassom em gatos na Vila Leopoldina disponibilizamos uma equipe de profissionais altamente competente a fim de prestar um ótimo atendimento em Cirurgia em Animais, Oncologia Animal, Clínica de Estética animal, Banho para gato e Exames laboratoriais para Cachorro. Com a ampla experiência que a equipe Dr Patinhas possui na atualidade, garantimos um constante desenvolvimento voltado a melhorar ainda mais nos destacados entre as principais empresas do mercado de Clinica Veterinária.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>