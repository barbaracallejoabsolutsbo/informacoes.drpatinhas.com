<?php
$title       = "Raio X em gatos em Embu Guaçú";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>No momento em que entrou em contato com a Dr. Patinhas, você notará que encontrou o melhor Raio X em gatos em Embu Guaçú. Com anos de experiência no ramo, fazemos sempre um bom trabalho e estamos sempre atentos às atualizações do mercado para fornecer o que há de melhor e mais moderno aos clientes. O raio X  se destina ao diagnóstico de doenças e outras anormalidades em animais, através de exames de imagem.</p>
<p>Especialista no mercado, a Dr Patinhas é uma empresa que ganha visibilidade quando se trata de Raio X em gatos em Embu Guaçú, já que possui mão de obra especializada em Castração de Gato, Internação para Cachorro, Hospital para Cachorro e gato, Internação para Gatos e Exames para Gatos. Nossa empresa vem crescendo e garantindo seu espaço entre as principais empresas do ramo de Clinica Veterinária, onde tem o foco em trazer o que se tem de melhor para seus clientes.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>