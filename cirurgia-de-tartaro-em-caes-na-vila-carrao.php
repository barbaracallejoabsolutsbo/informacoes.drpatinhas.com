<?php
$title       = "Cirurgia de tártaro em cães na Vila Carrão";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>De forma resumida, a Cirurgia de tártaro em cães na Vila Carrão é a especialidade médica que realiza procedimentos invasivos com finalidade terapêutica, e diagnóstica. Neste sentido, a Dr. Patinhas, com anos de experiência no ramo, realiza também cirurgias eletivas, emergenciais e de alta complexidade. E ainda, oferecemos todo o atendimento personalizado que o cliente e seu pet, procuram e merecem.</p>
<p>Procurando uma empresa de confiança onde você possa ter a garantia de satisfação em Cirurgia de tártaro em cães na Vila Carrão? A Dr Patinhas é o que você precisa! Sendo uma das principais empresas do ramo de Clinica Veterinária consegue proporcionar o melhor para seus clientes e parceiros, uma vez que, é especializada em Banho para cachorro, Veterinário Dermatológico, Acupuntura para animais, Tosa em Gatos e Banho para gato. Entre em contato e fique por dentro de mais informações.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>