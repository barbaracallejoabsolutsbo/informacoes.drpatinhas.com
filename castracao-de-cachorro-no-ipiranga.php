<?php
    $title       = "Castração de Cachorro no Ipiranga";
    $description = "Em suma, a castração de cachorro no Ipiranga evita também que haja aquela ninhada de filhotinhos para cuidar e ter que doar depois para outras pessoas.";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    <?php // include "includes/modal-orcamento-qsmi.php"; ?>

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php // echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>De forma resumida, a castração de cachorro no Ipiranga parece como a forma mais eficaz de prevenir e controlar importantes doenças, tais como: tumores, infecções, displasia, epilepsia, hemofilia, entre outros. Por isso, a Dr. Patinhas conta com profissionais qualificados para fornecer todo o atendimento personalizado que o seu pet procura e merece.</p>

<p>A castração de cachorro no Ipiranga mais procurada está aqui esperando por seu contato. Por isso, a qualquer hora do dia, nós estamos disponíveis para tirar todas as suas dúvidas e fornecer todo o suporte necessário, com presteza e atenção.</p>

<h2>Por que é importante a castração de cachorro no Ipiranga?</h2>

<p>O principal objetivo de castração de cachorro no Ipiranga  é, obviamente, prevenir que ele tenha filhotinhos e, para isso, o procedimento é extremamente efetivo. </p>
<p>Uma das principais características dos cachorros machos é demarcar o território com o xixi. Se o cachorro passar pela castração de cachorro no Ipiranga antes de completar um ano de idade, ele não demarcará território quando for adulto. Castrar é muito bom também para cachorros que já são adultos e que têm o hábito de demarcar território urinando pela casa, pois o hábito não vai se extinguir pelo seu cachorro já ter aprendido a demarcar território, mas pode diminuir muito.  </p>
<p>Lembrando que, a castração de cachorro no Ipiranga em fêmeas dá a elas menos chances de desenvolverem tumores na mama, com pesquisas comprovadas de que fêmeas castradas têm menor chance de desenvolverem tumores cancerígenos nas glândulas mamárias, em comparação com fêmeas não castradas.</p>
<p>Enquanto a castração de cachorro no Ipiranga  em machos também evita que apareçam tumores nos testículos e minimiza tumores na próstata. Ou seja, cachorros castrados, apresentam uma enorme diminuição no risco de desenvolvimento de algum tumor na próstata e durante o procedimento, anulam a possibilidade de desenvolvimento de tumores.</p>
<h2>Faça a castração de cachorro no Ipiranga com a melhor!</h2>
<p>Na Dr. Patinhas, realizamos cirurgias eletivas, emergenciais e de alta complexidade, além disso agregamos valores acessíveis e justos em conjunto com diversas formas de pagamento para facilitar a sua aquisição. E ainda, o nosso consultório é confortável e higienizado para melhor atendimento aos clientes e pets.</p>
<p>É importante destacar que, desde o início, é estabelecida uma relação de transparência e comprometimento para que ambas as partes se sintam seguras e confortáveis nesta parceria. Deixe os detalhes com a nossa equipe e desfrute de um trabalho bem feito. Ligue agora mesmo e saiba mais.</p>

                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>