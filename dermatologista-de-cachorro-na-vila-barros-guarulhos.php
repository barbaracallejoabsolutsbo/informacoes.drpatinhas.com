<?php
$title       = "Dermatologista de Cachorro na Vila Barros - Guarulhos";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Os deveres habituais de um Dermatologista de Cachorro na Vila Barros - Guarulhos na Saúde incluem avaliar o animal antes do tratamento, realizando uma inspeção completa. Durante a consulta, também é feita uma entrevista com o tutor, para maiores informações coletadas durante o exame clínico. Na maioria dos casos, eles são os principais responsáveis pelas doenças de pele. Entre em contato conosco para saber mais.</p>
<p>Com a Dr Patinhas proporcionando o que se tem de melhor e mais moderno no segmento de Clinica Veterinária consegue garantir aos seus clientes a confiança e conforto que todos procuram. Com o melhor em Médico Veterinário, Oftalmologia para animais, Laboratório para animais, Oftalmo para cachorros e Gatos e Hospital veterinário nossa empresa, hoje, consegue possibilitar diversas escolhas para os melhores resultados, ganhando destaque e se tornando referência em Dermatologista de Cachorro na Vila Barros - Guarulhos.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>