<?php
$title       = "Clínica de Estética animal no Ibirapuera";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Conte com a melhor Clínica de Estética animal no Ibirapuera. Com anos de experiência no ramo, não hesitamos em fazer um bom trabalho e buscamos superar as expectativas de nossos clientes, sempre mantendo o padrão de qualidade com atendimentos totalmente personalizados. Nos destacamos, pois temos o intuito de deixar o pet o mais calmo possível, para o contato com o veterinário na hora da consulta ser tranquilo.</p>
<p>Além de sermos uma empresa especializada em Clínica de Estética animal no Ibirapuera disponibilizamos uma equipe de profissionais altamente competente a fim de prestar um ótimo atendimento em Cirurgia em Animais, Internação para Gatos, Médico Veterinário, Melhor pet shop para banho e tosa e Oftalmo para cachorros e Gatos. Com a ampla experiência que a equipe Dr Patinhas possui na atualidade, garantimos um constante desenvolvimento voltado a melhorar ainda mais nos destacados entre as principais empresas do mercado de Clinica Veterinária.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>