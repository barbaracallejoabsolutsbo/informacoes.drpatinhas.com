<?php
$title       = "Emergência para animais no Ibirapuera";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Emergência para animais no Ibirapuera é com a Dr. Patinhas. Com anos de experiência, nós possuímos uma completa infraestrutura que permite atendimento rápido em casos de urgência, além da equipe de médicos veterinários treinada para qualquer situação. Nos destacamos, pois, oferecemos um atendimento personalizado a cada cliente e estamos sempre atentos as atualizações para fornecer serviços de última geração.</p>
<p>Por ser a principal empresa quando falamos de Clinica Veterinária, a Dr Patinhas se dispõe a adquirir os melhores e mais modernos recursos para atender seus clientes sempre da melhor forma. Possuindo como objetivo viabilizar tanto Ortopedia Animal Veterinário, Cirurgia de tártaro em cães, Ecocardiograma em Cachorro, Vacina contra Raiva e Oncologia Animal, quanto como Emergência para animais no Ibirapuera mantendo a qualidade e a eficiência que você deseja. Entre em contato e faça uma cotação.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>