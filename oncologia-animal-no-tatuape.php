<?php
$title       = "Oncologia Animal no Tatuapé";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>A Oncologia Animal no Tatuapé tem se despontado como a principal esperança para os tutores, pois, além da idade, existem outros fatores que podem levar ao desenvolvimento de câncer animais. Nossos especialistas em oncologia deve ser realizam exames corretos para detectar com precisão a enfermidade. Principalmente, quando diagnosticada precocemente, as chances do tratamento resultar em cura são bem maiores.</p>
<p>Especialista no segmento de Clinica Veterinária, a Dr Patinhas é uma empresa diferenciada, com foco em atender de forma qualificada todos os clientes que buscam por Oncologia Animal no Tatuapé. Trabalhando com o foco em proporcionar a melhores experiência para seus clientes, nossa empresa conta com um amplo catálogo para você que busca por Banho para cães e Gatos, Internação para Gatos, Clínica para Animais, Oncologia em cães e Gatos e Oftalmo para cachorros e Gatos e muito mais.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>