<?php
$title       = "Veterinário Dermatológico no Tremembé";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Pois bem, os deveres do Veterinário Dermatológico no Tremembé abrangem a avaliação geral do pet, antes mesmo de começar o tratamento, onde o profissional fará uma anamnese, entrevistando o tutor para que o mesmo informe tudo que ocorre com o animal. Problemas dermatológicos podem ter variadas causas, por isso demandam uma investigação completa sobre os hábitos de vida do animal. </p>
<p>Se destacando entre as mais competentes empresas do segmento de Clinica Veterinária, a Dr Patinhas vem proporcionando com total empenho seja em Veterinário Dermatológico no Tremembé quanto em Diagnóstico laboratório Veterinário, Raio X em gatos, Oncologia em cães e Gatos, Veterinário Dermatológico e Internação para Cachorro, com o foco em agregando qualidade e excelência em seu atendimento nossa empresa busca a constante melhoria e desenvolvimento de seus colaboradores de forma a garantir o melhor para quem nos busca.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>