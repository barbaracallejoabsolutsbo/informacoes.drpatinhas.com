<?php
$title       = "Diagnóstico laboratório Veterinário em Tanque Grande - Guarulhos";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>O Diagnóstico laboratório Veterinário em Tanque Grande - Guarulhos é de extrema importância para que a saúde dos pets seja mantida, já que muitas das doenças de animais têm um diagnóstico complicado de ser feito, e necessitam de exames laboratoriais para serem confirmadas. Mas não se preocupe, a nossa equipe oferece o suporte completo a sua necessidade, com presteza e atenção. Não deixe de conhecer nossos trabalhos.</p>
<p>Se deseja comprar Diagnóstico laboratório Veterinário em Tanque Grande - Guarulhos e procura por uma empresa séria e competente, a Dr Patinhas é a melhor opção. Com uma equipe formada por profissionais experientes e qualificados, dos quais trabalham para oferecer soluções diferenciadas para o projeto de cada cliente. Produtos Hydra para seu animal, Internação para Gatos, Oncologia Animal, Clínica de Pet Shop e Oftalmo para cachorros e Gatos. Entre em contato e saiba tudo sobre Clinica Veterinária com os melhores profissionais.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>