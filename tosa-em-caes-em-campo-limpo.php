<?php
$title       = "Tosa em cães em Campo Limpo";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>A Tosa em cães em Campo Limpo higiênica, por exemplo, é feita para manter a higiene e a limpeza do cachorro, ela consiste em aparar os pelos das patas e aparar a área íntima do cachorro, pois essa região acaba ficando com resquícios, muitas vezes de urina e fezes, concentrando um cheiro ruim e sujeira. A frequência para tosa vai variar de raça para raça, podendo ser no período de 45 dias a 3 meses. </p>
<p>Buscando por uma empresa de credibilidade no segmento de Clinica Veterinária, para que, você que busca por Tosa em cães em Campo Limpo, tenha a garantia de qualidade e idoneidade, contar com a Dr Patinhas é a opção certa. Aqui tudo é feito por um time de profissionais com amplo conhecimento em Atendimento para animais, Ecocardiograma em Cachorro, Internação para Cachorro, Cirurgia em Cachorros e Tosa em Gatos para assim, oferecer a todos os clientes as melhores soluções.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>