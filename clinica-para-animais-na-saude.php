<?php
    $title       = "Clínica para Animais na Saúde";
    $description = "A especialidade da clínica para animais na Saúde pode ser entendida como aquela destinada à consultas aos pets, tanto grandes quanto pequenos.
";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    <?php // include "includes/modal-orcamento-qsmi.php"; ?>

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php // echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>Se você está procurando por clínica para animais na Saúde está no lugar certo. A Dr. Patinhas após 6 anos de acompanhamento diário em cirurgias, enfermidades, distúrbios e disfunções apresentadas pelas diversas raças e espécies de animais, obteve a grande oportunidade de compartilhar seus conhecimentos de maneira totalmente independente, formando seu próprio centro de atendimento veterinário. </p>

<p>Atualmente, somos destaque como clínica para animais na Saúde, pois, não hesitamos em fazer um bom trabalho e contamos com profissionais qualificados para fornecer um atendimento personalizado ao cliente, bem como soluções completas, de forma rápida e eficiente.</p>

<h2>Como se destacar no ramo de clínica para animais na Saúde?</h2>

<p>Pois bem, uma boa clínica para animais na Saúde deve treinar médicos veterinários capacitados a realizar exames clínicos criteriosos, colheita de materiais biológicos, necropsias, cirurgia geral e específica, interpretação de exames laboratoriais, permitindo que seja oferecido serviço de qualidade por parte do profissional. </p>

<p>Como treinamento de qualidade, o profissional da clínica para animais na Saúde  é capaz de chegar a diagnósticos definitivos mais precisos e, consequentemente, oferecer tratamentos mais eficazes aos pacientes. Além disso, com o programa específico da área clínica para animais na Saúde, o mesmo é treinado a orientar o produtor quanto ao manejo de animais destinados à produção de carne ou leite, bovinos de elite, por exemplo. </p>
<p>Assim, o profissional formado e com a prática na clínica para animais na Saúde adequada estará em capacidade de dar apoio aos órgão públicos na identificação de focos de doenças, como febre aftosa, brucelose, raiva e tuberculose, além de contribuir no planejamento e execução de estratégias de prevenção e controle.</p>
<h2>Torne-se parceiro da melhor clínica para animais na Saúde!</h2>
<p>Conosco, você tem a tranquilidade de receber serviços de qualidade, através de profissionais qualificados. E ainda, nós agregamos o melhor custo benefício do mercado em conjunto com diversas formas de pagamento para facilitar a sua parceria.</p>
<p>Para melhor atender as necessidades de nossos pacientes, contamos com excelentes colaboradores que ao serem requisitados, prontamente se deslocam à nossa clínica para efetuarem os exames necessários. </p>
<p>Por fim, vale ressaltar que, dentre os diversos serviços que oferecemos, garantimos vacinas importadas, medicamentos, castração de animais, limpeza de tártaro, piometra e cesárea, além do excelente e cuidadoso tratamento no banho e tosa. Está esperando o que para ligar agora mesmo e se tornar parceiro da melhor clínica do ramo? Venha conferir e realizar um orçamento sem compromisso. A nossa equipe espera por você.</p>

                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>