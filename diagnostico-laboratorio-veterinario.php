<?php
    $title       = "Diagnóstico laboratório Veterinário";
    $description = "O diagnóstico laboratório veterinário preventivo permite que os clínicos e tutores descubram doenças antes que estas estejam em estágios avançados.
";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    <?php // include "includes/modal-orcamento-qsmi.php"; ?>

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php // echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>Há mais de 6 anos no ramo de diagnóstico laboratório veterinário, nos tornamos referência em qualidade, tecnologia e gestão na medicina veterinária laboratorial. Além disso, todos os profissionais do Dr. Patinhas oferece o atendimento personalizado que o cliente e pet procuram e merecem, a qualquer hora do dia.</p>
<p>O diagnóstico laboratório veterinário é de extrema importância para que a saúde dos pets seja mantida, já que muitas das doenças de animais têm um diagnóstico complicado de ser feito, e necessitam de exames laboratoriais para serem confirmadas. Mas não se preocupe, a nossa equipe oferece o suporte completo a sua necessidade, com presteza e atenção.</p>
<h2>Mais sobre diagnóstico laboratório veterinário:</h2>
<p>Dentro da gama de diagnóstico laboratório veterinário a ser realizado em um laboratório veterinário, exames de sangue dos mais variados tipos e exames de imagem como ultrassonografia e radiografia são os mais comuns.</p>
<p>Alguns laboratórios também já oferecem outros exames mais específicos como Eletrocardiografia e Ecocardiografia para diagnóstico laboratório veterinário cardiológico, além de ressonância magnética e tomografia computadorizada.</p>
<p>É importante frisar que, os exames laboratoriais veterinários nem sempre estiveram atrelados à rotina clínica do médico veterinário, pois, antigamente os veterinários clínicos contavam apenas com sua experiência para elaborar um diagnóstico laboratório veterinário.</p>
<p>Apesar de essencial e de extrema importância para o sucesso do diagnóstico laboratório veterinário, a experiência do veterinário não é suficiente para entender, por exemplo, quais nutrientes estão em deficiência no animal. Dessa forma, com os exames laboratoriais, é possível a realização de diagnósticos mais precisos e completos. </p>
<h2>Faça o diagnóstico laboratório veterinário ideal conosco!</h2>
<p>De forma resumida, nós possuímos uma completa infraestrutura que permite atendimento rápido em casos de urgência, além da equipe de médicos veterinários amplamente treinada para este tipo de situação. Além disso, agregamos o melhor custo benefício do mercado em conjunto com diversas formas de pagamento para facilitar a sua parceria.</p>
<p>Diante de todos esses fatores, ressaltamos que para melhor atender as necessidades de nossos pacientes, contamos com excelentes colaboradores que ao serem requisitados, prontamente se deslocam à nossa clínica para efetuarem os exames necessários. Se interessou? Não perca mais tempo e nem a oportunidade de se tornar parceiro de uma clínica que prioriza e respeita o seu pet. No momento em que ligar para a nossa equipe, você notará que encontrou a empresa ideal para atender o seu pet, com todo o amor necessário. Entre em contato agora mesmo e faça um orçamento sem compromisso.</p>

                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>