<?php
    $title       = "Veterinário Dermatológico no Ipiranga";
    $description = "É recomendado buscar um veterinário dermatológico no Ipiranga capacitado que prontamente saberá realizar os procedimentos adequados para um diagnóstico correto.
";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    <?php // include "includes/modal-orcamento-qsmi.php"; ?>

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php // echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>A Dr. Patinhas fornece veterinário dermatológico no Ipiranga, com um atendimento personalizado e soluções completas para a necessidade do seu pet. Além disso, a nossa equipe trabalha de forma unida e organizada para fornecer o que há de melhor e mais moderno aos clientes, de acordo com as constantes evoluções do mercado.</p>

<p>De forma resumida, o veterinário dermatológico no Ipiranga nada mais é que o médico responsável pela saúde de pele dos animais de estimação, no qual nesta área estão incluídos também os cuidados com as unhas e as orelhas.</p>
<h2>O que faz um veterinário dermatológico no Ipiranga?</h2>
<p>Pois bem, os deveres do veterinário dermatológico no Ipiranga abrangem a avaliação geral do pet, antes mesmo de começar o tratamento, onde o profissional fará uma anamnese, entrevistando o tutor para que o mesmo informe tudo que ocorre com o animal.</p>
<p>Problemas dermatológicos podem ter variadas causas, por isso demandam uma investigação completa sobre os hábitos de vida do animal. É também de responsabilidade do veterinário dermatológico no Ipiranga a coleta de amostras para exames, bem como a realização de cirurgias dermatológicas.</p>
<p>Por conta disso, um treinamento especializado do veterinário dermatológico no Ipiranga é estritamente necessário  para tratar problemas de difícil diagnóstico e para que ele seja realizado com rapidez, eficiência e precisão.</p>
<p>Vamos conhecer abaixo alguns serviços que o veterinário dermatológico no Ipiranga realiza:</p>
<ul>
<li>
<p>Raspagem de pele;</p>
</li>
<li>
<p>Citologia;</p>
</li>
<li>
<p>Teste de alergia;</p>
</li>
<li>
<p>Biópsia, entre outros.</p>
</li>
</ul>
<p>Por fim, a melhor opção para se tomar ao primeiro sinal de problema de pele é buscar um bom profissional especialista em dermatologia veterinária, evitando assim, que o problema se agrave.</p>
<h2>Dr. Patinhas - Veterinário dermatológico no Ipiranga confiável</h2>
<p>Após 6 anos de acompanhamento diário em cirurgias, enfermidades, distúrbios e disfunções apresentadas pelas diversas raças e espécies de animais, nós possuímos, atualmente, uma completa infraestrutura que permite atendimento rápido em casos de urgência, além da equipe de médicos veterinários amplamente treinada para este tipo de situação.</p>
<p>Diante de todos esses fatores, desde o início, estabelecemos uma relação de transparência e comprometimento para que ambas as partes se sintam seguras. Se interessou e quer saber mais? Ligue agora mesmo e realize um orçamento sem compromisso.</p>

                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>