<?php
$title       = "Castração de Gato na Vila Formosa";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>A Castração de Gato na Vila Formosa reduz acidentes, brigas e transmissão de doenças infecto-contagiosas, como por exemplo o vírus da imunodeficiência felina. Além disso, reduz o odor da urina dos felinos machos. De forma conclusiva, a castração pode ser realizada de forma eletiva em gatos adultos, desde que o animal não apresente condição de saúde restritiva que torne a cirurgia contraindicada.</p>
<p>Nós da Dr Patinhas trabalhamos dia a dia para garantir o melhor em Castração de Gato na Vila Formosa e para isso contamos com profissionais altamente capacitados para atender e garantir a satisfação de seus clientes e parceiros. Atuando no mercado de Clinica Veterinária com qualidade e dedicação, contamos com profissionais com amplo conhecimento em Exames para Cachorro, Oncologia Animal, Ortopedia Animal Veterinário, Médico Veterinário e Oftalmo para cachorros e Gatos e muito mais.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>