<?php
    $title       = "Oncologia em cães e Gatos";
    $description = "A melhor estratégia dos especialistas em oncologia em cães e gatos deve ser realizar os exames corretos para detectar com precisão a enfermidade.
";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    <?php // include "includes/modal-orcamento-qsmi.php"; ?>

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php // echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>Em geral, o profissional de oncologia em cães e gatos atua tanto com o diagnóstico de tumores quanto com a escolha do protocolo do tratamento de câncer e sua realização. Ao longo dos anos, este estudo tem crescido, tendo em vista que o objetivo é encontrar alternativas de tratamentos para os mais diferentes tipos de câncer que acometem os pets.</p>

<p>Sabendo dessa importância, a Dr. Patinhas não hesita em contar com profissionais qualificados de oncologia em cães e gatos garantindo o acompanhamento diário em cirurgias, enfermidades, distúrbios e disfunções apresentadas pelas diversas raças e espécies de animais.</p>

<h2>Informações sobre oncologia em cães e gatos:</h2>

<p>Embora a cirurgia muitas vezes seja o método escolhido para tratamento, há diversas alternativas para tratar oncologia em cães e gatos. Conheça as principais:</p>
<ul>
<li>
<p>Cirurgia: Remoção cirúrgica da massa tumoral, com o intuito de evitar metástase e melhorar a qualidade de vida do paciente;</p>
</li>
<li>
<p>Quimioterapia: Se baseia na administração de fármacos citotóxicos, que visam causar danos nas células tumorais, impedindo a sua replicação;</p>
</li>
<li>
<p>Imunoterapia: Consiste em estimular o sistema imunológico visando criar uma resposta antitumoral e consequente combate à doença de oncologia em cães e gatos, entre outros.</p>
</li>
</ul>
<p>Lembrando que, existe ainda o chamado tratamento paliativo, indicado para casos crônicos da oncologia em cães e gatos, em que o tumor está em caso avançado. Neste sentido, não há possibilidade de cura e os cuidados são apenas para aliviar os sofrimentos dos animais.</p>
<p>Por fim, é imprescindível que o profissional de oncologia em cães e gatos conheça as origens do problema e, principalmente, as formas de diagnóstico para que a doença seja diagnosticada com precisão o quanto antes.</p>
<h2>Tratamento de oncologia em cães e gatos é na Dr. Patinhas!</h2>
<p>Dentre os diversos serviços que oferecemos, podemos citar: vacinas importadas, medicamentos, castração de animais, limpeza de tártaro, piometra e cesárea, além do excelente e cuidadoso tratamento no banho e tosa. Além disso, agregamos valores acessíveis e justos em conjunto com diversas formas de pagamento para facilitar a sua parceria.</p>
<p>Vale frisar que, desde o primeiro contato, estabelecemos uma relação de transparência e comprometimento para que ambas as partes se sintam seguras e confortáveis neste momento de tratamento do pet. Venha conhecer melhor os nossos serviços e ter a certeza de que encontrou a clínica ideal para se tornar parceiro. Ligue agora mesmo, tire todas as suas dúvidas com a nossa equipe e faça um orçamento sem compromisso.</p>

                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>