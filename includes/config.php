<?php

    // Principais Dados do Cliente
    $nome_empresa = "Dr. Patinhas";
    $emailContato = "atendimento@drpatinhas.com";

    // Parâmetros de Unidade
    $unidades = array(
        1 => array(
            "nome" => "Dr. Patinhas",
            "rua" => "Avenida do Cursino, n 534",
            "bairro" => "Saúde",
            "cidade" => "São Paulo",
            "estado" => "São Paulo",
            "uf" => "SP",
            "cep" => "04133-000",
            "latitude_longitude" => "", // Consultar no maps.google.com
            "ddd" => "11",
            "telefone" => "2872-5755",
            "whatsapp" => "94069-4874",
            "link_maps" => "" // Incorporar link do maps.google.com
        ),
        2 => array(
            "nome" => "",
            "rua" => "",
            "bairro" => "",
            "cidade" => "",
            "estado" => "",
            "uf" => "",
            "cep" => "",
            "ddd" => "",
            "telefone" => ""
        )
    );
    
    // Parâmetros para URL
    $padrao = new classPadrao(array(
        // URL local
        "http://localhost/informacoes.drpatinhas.com/",
        // URL online
        "https://www.informacoes.drpatinhas.com/"
    ));
    
    // Variáveis da head.php
    $url = $padrao->url;
    $canonical = $padrao->canonical;
	
    // Parâmetros para Formulário de Contato
    $smtp_contato            = ""; // Solicitar ao líder do dpto técnico, 177.85.98.119
    $email_remetente         = ""; // Criar no painel de hospedagem, admin@...
    $senha_remetente         = "c0B1S3vH5eCvAO";

    // Contato Genérico (para sites que não se hospedam os e-mails)
    // $smtp_contato            = "111.111.111.111";
    // $email_remetente         = "formulario@temporario-clientes.com.br";
    // $senha_remetente         = "4567FGHJK";

    // Recaptcha Google
    $captcha                 = false; // https://www.google.com/recaptcha/
    $captcha_key_client_side = "";
    $captcha_key_server_side = "";

    // CSS default
    $padrao->css_files_default = array(
        "default/reset",
        "default/grid-system",
        "default/main",
        "default/slicknav-menu",
        "_main-style"
    );
    
    // JS Default
    $padrao->js_files_default = array(
        "default/jquery-1.9.1.min",
        "default/modernizr",
        "default/jquery.slicknav.min",
        "jquery.padrao.main"
    );
        
    // Listas de Palavras Chave
    $palavras_chave = array(
        "Acupuntura para animais no Sacomã",
"Atendimento para animais na Vila Mariana",
"Banho e tosa para animais",
"Banho para cachorro no Ipiranga",
"Banho para cães e Gatos",
"Banho para gato no Jabaquara",
"Castração de Cachorro no Ipiranga",
"Castração de Gato na Mooca",
"Cirurgia de tártaro em cães",
"Cirurgia em Animais no Sacomã",
"Cirurgia em Cachorros no Ipiranga",
"Clínica de Estética animal",
"Clínica de Pet Shop",
"Clínica para Animais na Saúde",
"Clínica veterinária de Domingo",
"Clínica veterinária noturna",
"Dermatologista de Cachorro na Saúde",
"Diagnóstico laboratório Veterinário",
"Ecocardiograma em Cachorro no Ipiranga",
"Emergência para animais na Vila Mariana",
"Exames laboratoriais para Cachorro",
"Exames para animais na Aclimação",
"Exames para Cachorro no Jabaquara",
"Exames para Gatos na Saúde",
"Hospital para Cachorro e gato",
"Hospital para cães e gatos",
"Hospital veterinário em São Paulo",
"Internação para Cachorro no Ipiranga",
"Internação para Gatos na Aclimação",
"Laboratório para animais na Vila Mariana",
"Médico Veterinário em São Paulo",
"Melhor pet shop para banho e tosa",
"Oftalmo para cachorros e Gatos",
"Oftalmologia para animais no Jabaquara",
"Oncologia Animal na Mooca",
"Oncologia em cães e Gatos",
"Ortopedia Animal Veterinário",
"Produtos Hydra para seu animal",
"Raio X em cachorro",
"Raio X em gatos",
"Tosa em cães no Sacomã",
"Tosa em Gatos no Ipiranga",
"Ultrassom em cachorro na Vila Mariana",
"Ultrassom em gatos na Saúde",
"Vacina contra Raiva",
"Vacinas para animais na Mooca",
"Valor Cesária cachorro e gato",
"Veterinário Dermatológico no Ipiranga"
    );
   
    $palavras_chave_com_descricao = array(
        "Item 1" => "Lorem ipsum dolor sit amet.",
        "Item 2" => "Laudem dissentiunt ut per.",
        "Item 3" => "Solum repudiare dissentiunt at qui.",
        "Item 4" => "His at nobis placerat.",
        "Item 5" => "Ei justo lucilius nominati vim."
    );
    
     /**
     * Submenu
     * 
     * $opcoes = array(
     * "id" => "",
     * "class" => "",
     * "limit" => 9999,
     * "random" => false
     * );
     * 
     * $padrao->subMenu($palavras_chave, $opcoes);
     * 
     */

    /**
     * Breadcrumb
     * 
     * -> Propriedades
     * 
     * Altera a url da Home no breadcrumb
     * $padrao->breadcrumb_url_home = "";
     * 
     * Altera o texto que antecede a Home
     * $padrao->breadcrumb_text_before_home = "";
     * 
     * Altera o texto da Home no breadcrumb
     * $padrao->breadcrumb_text_home = "Home";
     * 
     * Altera o divisor de níveis do breadcrumb
     * $padrao->breadcrumb_spacer = " » ";
     * 
     * -> Função
     * 
     * Cria o breadcrumb
     * $padrao->breadcrumb(array("Informações", $h1));
     * 
     */

    /**
     * Lista Thumbs
     * 
     * $opcoes = array(
     * "id" => "",
     * "class_div" => "col-md-3",
     * "class_section" => "",
     * "class_img" => "img-responsive",
     * "title_tag" => "h2",
     * "folder_img" => "imagens/thumbs/",
     * "extension" => "jpg",
     * "limit" => 9999,
     * "type" => 1,
     * "random" => false,
     * "text" => "",
     * "headline_text" => "Veja Mais"
     * );
     * 
     * $padrao->listaThumbs($palavras_chave, $opcoes);
     * 
     */
    
    /**
     * Funções Extras
     * 
     * $padrao->formatStringToURL();
     * Reescreve um texto em uma URL válida
     * 
     */