<?php
    $title       = "Oftalmologia para animais no Jabaquara";
    $description = "Na Dr. Patinhas, a oftalmologia para animais no Jabaquara conta com uma equipe unida e organizada para trabalhar de forma rápida e eficiente no tratamento de seu pet.
";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    <?php // include "includes/modal-orcamento-qsmi.php"; ?>

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php // echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>A oftalmologia para animais no Jabaquara é muito comum e freqüente na ocorrência de afecções oculares nas espécies domésticas como pequenos animais, por exemplo. Neste sentido, a Dr. Patinhas se tornou referência por apresentar soluções completas, de forma rápida e eficiente. Além disso, todos os nossos profissionais passam por um treinamento adequado para fornecer o atendimento personalizado que o cliente procura e merece.</p>

<p>Quem procura por oftalmologia para animais no Jabaquara, conte conosco, pois, nós fazemos questão de representar um ramo da medicina responsável pelo diagnóstico e tratamento de enfermidades nos olhos e em seus anexos, com todo o cuidado necessário. Além disso, a qualquer hora do dia, nós estamos disponíveis para tirar todas as suas dúvidas.</p>

<h2>A importância da oftalmologia para animais no Jabaquara:</h2>

<p>O veterinário especializado em oftalmologia para animais no Jabaquara  atende principalmente pequenos animais, como cães e gatos, fazendo o diagnóstico e o tratamento de todas as doenças que atingem os olhos, seus tecidos e anexos.</p>
<p>Se faz necessário que as visitas sejam constantes ao oftalmologia para animais no Jabaquara para a realização de check-ups oculares e também são essenciais nos casos em que os animais apresentam sintomas como alterações oculares, excesso de remela, vermelhidão, irritação ocular, entre outros fatores.</p>
<p>Na prática da oftalmologia para animais no Jabaquara, o pet recebe um atendimento abrangente e cuidadoso, com o auxílio dos melhores equipamentos para diagnósticos do mercado, de modo a acompanhar os animais desde a identificação das doenças oculares até a conclusão do tratamento. </p>
<p>É importante salientar que, o diagnóstico precoce das doenças oculares é fundamental para aumentar as chances de cura e para evitar que o pet desenvolva cegueira parcial ou total e outros quadros mais graves. Por isso, ao perceber qualquer comportamento diferente de seu pet em relação a visão, agende uma consulta de oftalmologia para animais no Jabaquara.</p>
<h2>A melhor oftalmologia para animais no Jabaquara:</h2>

<p>A Dr. Patinhas trabalha com vacinas importadas, medicamentos, castração de animais, limpeza de tártaro, piometra e cesárea, além do excelente e cuidadoso tratamento no banho e tosa. Além disso, agregamos valores acessíveis e justos em conjunto com diversas formas de pagamento para facilitar a sua contratação e parceria.</p>
<p>No momento em que entrar em contato conosco, você notará que encontrou a clínica ideal para se tornar o mais novo parceiro de longa data. Deixe os detalhes conosco e desfrute de um trabalho bem feito. Ligue agora mesmo e saiba mais.</p>


                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>